const
  path = require('path'),
  child = require('child_process'),
  webpack = require('webpack'),
  _ = require('lodash'),
  { execSync } = require('child_process');

module.exports = {
  mode: 'development',
  entry: path.resolve('./src/index.js'),
  output: {
    library: 'ssvg',
    libraryTarget: 'window',
    path: path.resolve(__dirname, './dist'),
    publicPath: '/dist/',
    filename: 'ssvg-engine.js'
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: [ 'style-loader', 'css-loader' ],
      },
      {
        test: /\.scss$/,
        use: [ 'style-loader', 'css-loader', 'sass-loader' ]
      },
      {
        test: /\.js$/,
        loader: 'babel-loader',
        options: {
          extends: path.join(__dirname, '.babelrc')
        }
      }
    ]
  },
  resolve: {
    alias: {
      'd3-transition': 'd3-transition/dist/d3-transition.min.js',
      'd3-scale': 'd3-scale/dist/d3-scale.min.js',
      'd3-selection': 'd3-selection/dist/d3-selection.min.js',
      'd3-interpolate': 'd3-interpolate/dist/d3-interpolate.min.js',
      'd3-array': 'd3-array/dist/d3-array.min.js'
    },
    extensions: [ '.js', '.json' ]
  },
  devServer: {
    historyApiFallback: true,
    noInfo: true,
    overlay: true
  },
  performance: {
    hints: "warning"
  },
  devtool: 'inline-cheap-module-source-map',
  plugins: [
    new webpack.ProvidePlugin({
      Buffer: [ 'buffer', 'Buffer' ]
    }),
    new webpack.DefinePlugin({
      VERSION: JSON.stringify(child.execSync('git describe --tags --always HEAD').toString().trim())
    }),
    {
      apply: (compiler) => {
        compiler.hooks.beforeCompile.tap('IstanbulPatch', () => {
          /* see https://github.com/istanbuljs/nyc/issues/718 for details */
          execSync("sed -i='tmp' 's/source: pathutils.relativeTo(start.source, origFile),/source: origFile,/' node_modules/istanbul-lib-source-maps/lib/get-mapping.js")
        });
      }
    }
  ]
};

if (process.env.NODE_ENV === 'production') {
  module.exports.devtool = 'source-map';
  module.exports.mode = 'production';
  _.set(module.exports, 'optimization.nodeEnv', 'production');
  _.set(module.exports, 'optimization.moduleIds', 'named');
}
