# Stateful Scalar Vector Graphics engine

SSVG engine is designed to animate SVG documents, as describbed by the [SSVG specification](https://mro-dev.web.cern.ch/docs/std/ssvg-specification.html)

An example/playground application is available [here](https://mro-dev.web.cern.ch/ssvg-engine)

# Build

To build the library (assuming that Node.js is installed on your machine):
```bash
# Run webpack and bundle things in /dist
npm run build

# Serve pages on port 8080
npm run serve
```

# Usage

To use this library as a dependency:
```bash
# Install it
npm install "git+https://gitlab.cern.ch/apc/common/www/ssvg-engine.git"
```

```javascript
import { SSVGEngine } from 'ssvg-engine';

/* create and engine hooking it on a DOM element */
const engine = new SSVGEngine(window.getElementById('svg'));

/* update the engine state */
engine.updateState({ range: 12 });

/* dispose the object */
engine.disconnect();
```

**Note:** You can use the engine as an external (CDN like) dependency, either [manually](./example/basic-manual.html) or as a [plugin](./example/basic-plugin.html).
