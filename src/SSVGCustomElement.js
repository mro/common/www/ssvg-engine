// @ts-check

import { select } from 'd3-selection';
import d from 'debug';
const debug = d('ssvg:engine');

import SSVGEngine from './SSVGEngine';

export default class SSVGCustomElement extends HTMLElement {
  constructor() {
    super();
    this._listener = new MutationObserver(this.load.bind(this));
    this._listener.observe(this, { childList: true });
    /** @type {SSVGEngine|null} */
    this._ssvg = null;
    /** @type {ssvg.StateSelection|null} */
    this._states = null;
    this.load();
  }

  disconnect() {
    this._listener.disconnect();
    if (this._ssvg) {
      this._ssvg.disconnect();
      this._ssvg = null;
    }
  }

  load() {
    if (this._ssvg) {
      this._ssvg.disconnect();
      this._ssvg = null;
    }
    const svg = select(this).select('svg');
    if (!svg.empty()) {
      // @ts-ignore: checked on empty() call
      this._ssvg = new SSVGEngine(svg.node());
      this._states = svg.selectAll('state');
    }
  }

  static register() {
    debug('registering s-svg custom Elements');
    window.customElements.define("s-svg", SSVGCustomElement);
  }

  /**
   * @param  {ssvg.$State} state
   */
  updateState(state /*: {} */) {
    if (!this._states) { return; }
    this._states.each(function() { this.updateState(state); });
  }

  /**
   * @param {ssvg.$State} state
   */
  setState(state /*: {} */) {
    if (!this._states) { return; }
    this._states.each(function() { this.setState(state); });
  }

  getState() {
    if (!this._states) { return; }
    const node = this._states.node();
    if (node) {
      return node.getState();
    }
    return undefined;
  }
}
