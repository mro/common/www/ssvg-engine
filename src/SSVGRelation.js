// @ts-check

import { TType } from './parser/values';
import { TransitionType } from './SSVGTransition';

import 'd3-transition'; // needed at least once to load plugins

import { scaleLinear } from 'd3-scale';
import { interpolateNumber } from 'd3-interpolate';
import { bisect } from 'd3-array';
import { AttributeType, CalcMode, default as SSVGRelationElement } from './SSVGRelationElement';
import SSVGFilter from './SSVGFilter';

/**
 * @typedef {import('./SSVGState').default} SSVGState
 * @typedef {import('./SSVGProperty').default} SSVGProperty
 */

/**
 * @template T=any
 * @param {number[]} keyTimes
 * @param {T[]} values
 * @return {(t: number) => T}
 */
export function scaleThreshold(keyTimes, values) {
  return function(t) {
    return values[bisect(keyTimes, t, 1, keyTimes.length) - 1];
  };
}

export default class SSVGRelation extends SSVGRelationElement {
  /**
   *
   * @param {SSVGState} ssvg
   * @param {Element} element
   */
  constructor(ssvg, element) {
    super(ssvg, element);
    this.id = ++SSVGRelation.id;

    /** @type {(t: number) => ssvg.$Value} */
    this.scale; /* eslint-disable-line no-unused-expressions *//* jshint ignore:line */

    /** @type {number[]|null} */
    this.keyTimes; /* eslint-disable-line no-unused-expressions *//* jshint ignore:line */

    /** @type {undefined|null|((prop: SSVGProperty, t?: number) => number)} */
    this.quantize; /* eslint-disable-line no-unused-expressions *//* jshint ignore:line */

    /** @type {ssvg.$Value} */
    this._last; /* eslint-disable-line no-unused-expressions *//* jshint ignore:line */

    this._filter = new SSVGFilter(element);
    this.init();
  }

  init() {
    this.animName = 'ssvg-rel-' + this.id;
    this.canInterpolate = TType[this.values.type].canInterpolate;

    if (!this.keyTimes) {
      this.keyTimes =
        this.values.values.map((val, idx) => idx / (this.values.values.length - 1));
    }
    if (!this.canInterpolate) {
      this.scale = scaleThreshold(
        this.keyTimes,
        this.values.values);
    }
    else {
      this.scale = scaleLinear(this.keyTimes, this.values.values);

      if (this.calcMode === CalcMode.discrete) {
        const toScale = scaleThreshold(this.keyTimes, this.keyTimes);
        this.quantize = (prop, t) => toScale(prop.normalize(t));
      }
    }
  }

  disconnect() {
    this.selection.interrupt(this.animName);
  }

  /**
   * @param {SSVGProperty} prop
   */
  setInitial(prop) {
    this.selection.interrupt(this.animName);
    const to = this.quantize ? this.quantize(prop) : prop.normalize();
    const value = (this.transition.type === TransitionType.direct) ?
      this.direct(this.scale(to)) : this.strict(to);
    if (!this.attributeName) {
      this.selection.text(value);
    }
    else if (this.attributeType === AttributeType.CSS) {
      this.selection.style(this.attributeName, value);
    }
    else {
      this.selection.attr(this.attributeName, value);
    }
  }

  /**
   * @param {SSVGProperty} prop
   */
  update(prop) {
    this.selection.interrupt(this.animName);

    const to = (this.quantize) ? this.quantize(prop) : prop.normalize();
    var t = this.selection.transition(this.animName);
    if (this.transition.delay) {
      t = t.delay(this.transition.delay);
    }
    t = t.ease(this.transition.timingFunction);

    t = t.duration(this.transition.duration);
    /** @type {(t: number) => any} */
    var factory;
    if (this.transition.type === TransitionType.direct) {
      const scaledTo = this.scale(to);
      // non-interpolatable direct transitions change value at half animation
      // duration
      const interpolator = (this.canInterpolate) ?
        scaleLinear([ 0, 1 ], [ this._last, scaledTo ]) :
        (/** @type {number} */t) => ((t >= 0.5) ? scaledTo : this._last);
      factory = (t) => this.direct(interpolator(t));
    }
    else {
      // @ts-ignore
      const interpolator = interpolateNumber(this._last, to);
      factory = (t) => this.strict(interpolator(t));
    }

    if (!this.attributeName) {
      t.textTween(() => factory);
    }
    else if (this.attributeType === AttributeType.CSS) {
      t.styleTween(this.attributeName, () => factory);
    }
    else {
      t.attrTween(this.attributeName, () => factory);
    }
  }

  /**
   * @brief computed target value from normalized input
   * @param {number} n
   */
  strict(n) {
    this._last = n;
    const value = this.scale(n);
    return this._filter.filter(value, this.values.unit);
  }

  /**
   * @brief pre-computed target value
   * @param {ssvg.$Value} value
   */
  direct(value) {
    this._last = value;
    return this._filter.filter(value, this.values.unit);
  }
}

SSVGRelation.id = 0;
