// @ts-check

import SSVGProperty from './SSVGProperty';
import { defaultTo, isNil } from './utils';
import d from 'debug';
const debug = d('ssvg:prop');

/**
 * @typedef {import('./SSVGState').default} SSVGState
 */

export default class SSVGPropertyNumber extends SSVGProperty {
  /**
   * @param {SSVGState} ssvg
   * @param {Element} element
   */
  constructor(ssvg, element) {
    super(ssvg, element);

    this.min = Number.parseFloat(element.getAttribute('min') || '0');
    this.max = Number.parseFloat(element.getAttribute('max') || '1');
    // @ts-ignore: of course we can parseFloat a number
    this._value = Number.parseFloat(defaultTo(element.getAttribute('initial'), this.min));
    if (Number.isNaN(this.min)) {
      throw new SyntaxError('invalid min value for number property: ' +
        (element.getAttribute('min') || ''));
    }
    else if (Number.isNaN(this.max)) {
      throw new SyntaxError('invalid max value for number property: ' +
        (element.getAttribute('max') || ''));
    }
    for (let i = 0; i < this.relations.length; ++i) {
      this.relations[i].setInitial(this);
    }
  }

  /**
   * @param {number?=} [value]
   * @return {number}
   */
  normalize(value) {
    if (this.min === this.max) {
      return this.min;
    }
    else if (!isNil(value)) {
      // @ts-ignore
      return (Number.parseFloat(value) - this.min) / (this.max - this.min);
    }
    else if (isNil(this._normalized)) {
      this._normalized = (this._value - this.min) / (this.max - this.min);
    }
    return this._normalized;
  }

  /** @param {number|any} value */
  set value(value) {
    this._normalized = null;
    var num = Number.parseFloat(value);
    if (Number.isNaN(num)) {
      debug('invalid number %o for property %s', value, this.name);
      num = this.min;
    }
    super.value = num;
  }

  /** @return {number|any} */
  get value() {
    return super.value;
  }
}
