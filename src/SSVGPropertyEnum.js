// @ts-check

import SSVGProperty from './SSVGProperty';
import { isNil } from './utils';
import { parseInitial, parseString } from './parser/values';
import { parseList } from './parser/utils';

/**
 * @typedef {import('./SSVGState').default} SSVGState
 */

export default class SSVGPropertyEnum extends SSVGProperty {
  /**
   * @param {SSVGState} ssvg
   * @param {Element} element
   */
  constructor(ssvg /*: SSVGState */, element /*: Element */) {
    super(ssvg, element);
    const values = element.getAttribute('values');
    if (!values) {
      throw new SyntaxError('values missing on enum property');
    }
    this.values = parseList({ index: 0, value: values }, parseString);
    if (this.values.length === 0) {
      throw new SyntaxError('invalid values property on enum: ' + values);
    }
    /** @type {{ [key: string]: number }} */
    this.range = {};
    const count = this.values.length;
    if (count === 1) {
      this.range[this.values[0]] = 0;
    }
    else {
      for (let i = 0; i < count; ++i) {
        this.range[this.values[i]] = i / (count - 1);
      }
    }

    this._value = this.values[0];
    const initial = element.getAttribute('initial');
    if (!isNil(initial)) {
      this._value = parseInitial(initial);
    }

    for (let i = 0; i < this.relations.length; ++i) {
      this.relations[i].setInitial(this);
    }
  }

  /**
   * @param {string?=} value
   * @return {number}
   */
  normalize(value) {
    if (isNil(value)) {
      // @ts-ignore
      return this.range[this._value];
    }
    return this.range[value];
  }

  /** @param {string|any} value */
  set value(value) {
    if (!isNil(this.range[value])) {
      super.value = value;
    }
  }

  /** @return {string|any} */
  get value() {
    return super.value;
  }
}
