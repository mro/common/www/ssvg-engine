// @ts-check

import SSVGProperty from './SSVGProperty';
import { parseInitial } from './parser';
import { defaultTo, isNil } from './utils';

/**
 * @typedef {import('./SSVGState').default} SSVGState
 */

export default class SSVGPropertyBoolean extends SSVGProperty {
  /**
   * @param {SSVGState} ssvg
   * @param {Element} element
   */
  constructor(ssvg, element) {
    super(ssvg, element);
    const initial = defaultTo(element.getAttribute('initial'), 'false');
    this._value = !!parseInitial(initial);

    for (let i = 0; i < this.relations.length; ++i) {
      this.relations[i].setInitial(this);
    }
  }

  /** @param {any} value */
  normalize(value /*: any */) {
    if (isNil(value)) {
      return this._value ? 1 : 0;
    }
    return value ? 1 : 0;
  }

  /** @param {boolean|any} value */
  set value(value /*: any */) {
    super.value = value ? true : false;
  }

  /** @return {boolean|any} */
  get value() {
    return super.value;
  }
}
