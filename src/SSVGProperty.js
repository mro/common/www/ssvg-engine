// @ts-check

import SSVGRelation from './SSVGRelation';
import SSVGTransform from './SSVGTransform';
import SSVGDirect from './SSVGDirect';

/**
 * @typedef {import('./SSVGState').default} SSVGState
 */

/** @type {{ [key: string]: number }} */
export const PropertyType = {
  boolean: 0,
  number: 1,
  enum: 2,
  auto: 3
};

export default class SSVGProperty {
  /**
   * @param {SSVGState} ssvg
   * @param {Element} element
   */
  constructor(ssvg, element) {
    this.ssvg = ssvg;
    this.name = element.getAttribute('name') || '';
    this.type = PropertyType[element.getAttribute('type') || 'boolean'];

    /** @type {SSVGRelation[]} */
    this.relations = [];

    for (let i = 0; i < element.children.length; ++i) {
      const relation = element.children[i];
      if (relation.tagName === 'relation') {
        this.makeRelation(relation);
      }
      else if (relation.tagName === 'transform') {
        this.makeTransform(relation);
      }
      else if (relation.tagName === 'direct') {
        this.makeDirect(relation);
      }
    }
  }

  disconnect() {
    for (const relation of this.relations) {
      relation.disconnect();
    }
  }

  /** @param {Element} relation */
  makeTransform(relation /*: Element */) {
    this.relations.push(new SSVGTransform(this.ssvg, relation));
  }

  /** @param {Element} relation */
  makeRelation(relation /*: Element */) {
    this.relations.push(new SSVGRelation(this.ssvg, relation));
  }

  /** @param {Element} relation */
  makeDirect(relation /*: Element */) {
    this.relations.push(new SSVGDirect(this.ssvg, relation));
  }

  /** @return {any} */
  get value() {
    return this._value;
  }

  /** @param {any} value */
  set value(value /*: any */) {
    if (value !== this._value) {

      /* FIXME: check for changes */
      this._value = value;
      for (const relation of this.relations) {
        relation.update(this);
      }
    }
  }

  /**
   * @param  {any} [value]
   * @return {number}
   */ // eslint-disable-next-line @typescript-eslint/no-unused-vars
  normalize(value) { return 0; } // jshint ignore:line
}
