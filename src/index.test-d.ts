
import { expectType } from 'tsd';

import { SSVGEngine, plugin, parser, isCSS } from '.';
import { $State, SSVGState } from '.';

(async function() {
  var elt = document.getElementById('#test');
  if (!elt) { return; }
  const engine = new SSVGEngine(elt);
  engine.updateState({ test: 42 });
  engine.setState({ test: 42 });
  expectType<$State>(engine.getState());
  expectType<Element>(engine.svg);
  expectType<SSVGState[]>(engine.states);

  plugin();

  expectType<boolean>(isCSS("test"));

  const transform = parser.parseTTransform('test')
  if (transform) {
    expectType<number[]>(transform[0].args)
  }

  parser.skipSpaces({ index: 0, value: '  ' })

  parser.parseList<number>({ index: 0, value: '  ' }, parser.parseScalar)
}());