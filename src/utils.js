// @ts-check

/**
 * @template T=any
 * @param {T|null|undefined} value
 * @param {T} def
 * @return {T}
 */
export function defaultTo(value, def) {
  return (value === undefined || value === null) ? def : value;
}

/**
 * @param {any} value
 * @return {value is null | undefined}
 */
export function isNil(value /*: any */) /*: boolean */ {
  return (value === undefined) || (value === null);
}

export const argSepRe = new RegExp('\\s*[\\s,]\\s*');
