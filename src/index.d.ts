
export = ssvg;
export as namespace ssvg;

declare namespace ssvg {
  class SSVGEngine {
    constructor(svg: Element)
    disconnect(): void
    updateState(state: $State): void
    setState(state: $State): void
    getState(): $State

    svg: Element
    selection: any
    states: SSVGState[]
  }

  interface $State {
    [property: string]: any
  }
  
  function plugin(): void

  namespace parser {
    function parseTTransform(value: string): $Transform[]|null
    function parseTTransformRange(value: string): $Transform[][]|null
    function normalizeTransform(transform: $Transform[][]): $TransformRange[]
    function skipSpaces(ctx: ParseCtx): string|null
    function makeError(message: string, ctx: ParseCtx): string
    function parseList<T>(ctx: ParseCtx, func: (ctx: ParseCtx) => T): T[]
    function parseDomCallback(str: string, args: string[]): Function
    
    const TType: {
      color: { canInterpolate: boolean },
      number: { canInterpolate: boolean },
      string: { canInterpolate: boolean }
    }

    function parseString(ctx: ParseCtx): string
    function parseScalar(ctx: ParseCtx): number
    function parseTRange(value: string): $TRange|null
    function parseTValue(value: string): $TValue|null
    function makeRange(from: $TValue|null|undefined, to: $TValue|null|undefined): $TRange

    interface ParseCtx {
      index: number,
      value: string,
      unit?: string
    }
  }
  
  function isCSS(name: string): boolean

  interface $Transform {
    transform: string,
    units: Array<string|null>
    args: number[]
  }

  interface $TransformRange {
    transform: string
    args: Array<number[]>
    units: Array<string|null>
  }
  
  type $Value = string | number
  type $TTypeName = 'color'|'number'|'string'
  interface $TValue {
    value: $Value,
    type: $TTypeName,
    unit?: string
  }
  interface $TRange {
    values: $Value[],
    type: $TTypeName,
    unit?: string
  }

  interface SSVGState {
    constructor(svg: Element): SSVGState
    disconnect(): void
    updateState(state: $State): void
    setState(state: $State): void
    getState(): $State
  }

  /* extended DOM elements */
  interface SSVGStateElement extends Element {
    updateState(state: $State): void
    setState(state: $State): void
    getState(): $State
  }

  interface SSVGCustomElement extends Element {
    updateState(state: $State): void
    setState(state: $State): void
    getState(): $State
  }
}
