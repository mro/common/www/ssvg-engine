// @ts-check

import d from 'debug';

const debug = d('ssvg:parser');

/**
 * @param  {ParseCtx} ctx
 * @return {string|null}
 */
export function skipSpaces(ctx) {
  for (var i = ctx.index; i < ctx.value.length; ++i) {
    const c = ctx.value[i];
    if (c !== ' ' && c !== '\t' && c !== '\n') {
      ctx.index = i;
      return c;
    }
  }
  ctx.index = ctx.value.length;
  return null;
}

/**
 * @param {string} message
 * @param {ParseCtx} ctx
 * @return {string}
 */
export function makeError(message, ctx) {
  var out = message + '\n' + ctx.value + '\n' + '-'.repeat(ctx.index) + '^\n';
  debug('SyntaxError: ' + out);
  return out;
}

/**
 * @template T
 * @param {ParseCtx} ctx
 * @param {(ctx: ParseCtx) => T} func
 * @return {T[]}
 */
export function parseList(ctx, func) {
  /** @type {T[]} */
  var values = [];

  while (ctx.index < ctx.value.length) {
    values.push(func(ctx));
    var c = skipSpaces(ctx);
    if (c !== null) {
      if (c !== ';') {
        throw new SyntaxError(makeError(`invalid list separator '${c}'`, ctx));
      }
      ++ctx.index;
    }
  }
  return values;
}

/**
 * @param {string} str
 * @param {string[]} args
 * @param {Window} win
 * @return {Function}
 * @details win may differ from global window when using iframes
 */
export function parseDomCallback(str, args, win = window) {
  // @ts-ignore
  if (win.hasOwnProperty(str) && typeof win[str] === 'function') {
    // @ts-ignore
    return win[str];
  }

  /* eslint-disable-next-line no-new-func */ // @ts-ignore
  var fun = new win.Function(...args, "return " + str);
  /** @type {boolean} */
  var direct;
  return function(/** @type {any[]} */ ...args) {
    if (direct === true) {
      // @ts-ignore
      return fun.call(this, ...args);
    }
    else if (direct === false) {
      // @ts-ignore
      return fun.call(this, ...args).call(this, ...args);
    }
    else { // first function call, let's check result
      // @ts-ignore
      const ret = fun.call(this, ...args);
      direct = ((typeof ret) !== 'function');

      return (direct === false) ? // @ts-ignore
        (ret.call(this, ...args)) : ret;
    }
  };
}
