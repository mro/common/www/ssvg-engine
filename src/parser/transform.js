// @ts-check

import { makeError, parseList, skipSpaces } from './utils';
import { parseScalar } from './values';

/**
 * @param       {ParseCtx} ctx
 * @return {string|null}
 */
// eslint-disable-next-line complexity
function _parseName(ctx) {
  var i;
  for (i = ctx.index; i < ctx.value.length; ++i) {
    const c = ctx.value.charCodeAt(i);
    if (c === 0x20 || c === 0x09 || c === 0x28 || c === 0x0A) { /* ' ' '\t' '(' '\n' */
      break;
    }
    else if ((c < 0x41 || c > 0x5A) && (c < 0x61 || c > 0x7A) && (c < 0x30 ||  c > 0x39)) { /* [a-z] [A-Z] [0-9] */
      ctx.index = i;
      throw new SyntaxError(makeError("failed to parse invalid characters in transform name", ctx));
    }
  }
  const name = ctx.value.slice(ctx.index, i);
  ctx.index = i;
  return name;
}

/**
 * @param {ParseCtx} ctx
 * @param {ssvg.$Transform} transform
 * @return {ssvg.$Transform}
 */
function _parseTransformArgs(ctx, transform) {
  while (ctx.index < ctx.value.length) {
    delete ctx.unit;
    const value = parseScalar(ctx);
    transform.units.push(ctx.unit || null);
    transform.args.push(value);

    var c = skipSpaces(ctx);
    if (c === ',') {
      ctx.index += 1;
      skipSpaces(ctx);
    }
    else if (c === ')') {
      ctx.index += 1;
      return transform;
    }
  }
  throw new SyntaxError(makeError('failed to parse unterminated transform', ctx));
}

/**
 * @param {ParseCtx} ctx
 * @return {ssvg.$Transform|null}
 */
function _parseTransformSingle(ctx) {
  if (skipSpaces(ctx) === null) {
    return null;
  }
  var transform = _parseName(ctx);
  if (!transform) {
    throw new SyntaxError(makeError('failed to parse unnamed transform', ctx));
  }
  else if (skipSpaces(ctx) !== '(') {
    throw new SyntaxError(makeError('failed to parse transform arguments', ctx));
  }
  ctx.index += 1;
  return _parseTransformArgs(ctx, { transform, args: [], units: [] });
}

/**
 * @param {ParseCtx} ctx
 * @return {ssvg.$Transform[]}
 */
function _parseTransform(ctx) {
  var values = [];

  while (ctx.index < ctx.value.length) {
    var transform = _parseTransformSingle(ctx);
    if (!transform) { break; }

    values.push(transform);
    var c = skipSpaces(ctx);
    if (c !== null) {
      if (c === ';') {
        break;
      }
      else if (c === ',') {
        ++ctx.index;
      }
      skipSpaces(ctx);
    }
  }
  return values;
}

/**
 * @param {string} value
 * @return {ssvg.$Transform[]|null}
 */
export function parseTTransform(value) {
  const ctx = { index: 0, value: value };
  var c = value ? skipSpaces(ctx) : null;
  if (c === null) {
    return null;
  }
  const values = _parseTransform(ctx);
  if (ctx.value[ctx.index] === ';') {
    throw new SyntaxError(makeError("invalid separator in single transform ';'", ctx));
  }
  return values;
}

/**
 * @param {string} value
 * @return {ssvg.$Transform[][]|null}
 */
export function parseTTransformRange(value) {
  /** @type {ParseCtx} */
  const ctx = { index: 0, value: value };
  var c = value ? skipSpaces(ctx) : null;
  if (c === null) {
    return null;
  }
  return parseList({ index: 0, value: value }, _parseTransform);
}

/**
 * @param {ssvg.$TransformRange} range
 * @param {ssvg.$Transform} tr
 */
function _updateArgs(range, tr) {
  var check;
  if (tr.units.length > range.units.length) {
    check = range.units;
    range.units = tr.units;
  }
  else {
    check = tr.units;
  }
  for (let i = 0; i < check.length; ++i) {
    if (!range.units[i]) {
      range.units[i] = check[i];
    }
    else if (!check[i]) {
      check[i] = range.units[i];
    }
    else if (check[i] !== range.units[i]) {
      throw new SyntaxError(`${range.transform} transform unit mismatch "${range.units[i] || ''}" != "${check[i] || ''}"`);
    }
  }
  range.args.push(tr.args);
}

/**
 * @param {ssvg.$TransformRange} range
 */
function _padArgs(range) {
  const max = range.units.length;
  for (let i = 0; i < range.args.length; ++i) {
    const len = range.args[i].length;
    if (len < max) {
      range.args[i].length = max;
      range.args[i].fill(0, len);
    }
  }
}

/**
 * @param {ssvg.$Transform[][]} transform
 * @return {ssvg.$TransformRange[]}
 */
export function normalizeTransform(transform) {
  /** @type {ssvg.$TransformRange[]} */
  var ret = [];
  var transformCount = transform.reduce((max, tr) => Math.max(max, tr.length), 0);

  for (let stepIdx = 0; stepIdx < transform.length; ++stepIdx) {
    const step = transform[stepIdx];
    for (let trIdx = 0; trIdx < transformCount; ++trIdx) {
      const tr = step[trIdx];
      const range = ret[trIdx];
      if (!range) {
        if (!tr) {
          ret[trIdx] = { transform: '', args: [ [] ], units: [ ] };
        }
        else {
          // @ts-ignore
          ret[trIdx] = step[trIdx];
          ret[trIdx].args = [ step[trIdx].args ];
        }
      }
      else if (!tr) {
        range.args.push([]);
      }
      else {
        if (!range.transform) {
          range.transform = tr.transform;
        }
        else if (range.transform !== tr.transform) {
          throw new SyntaxError(`transform name mismatch "${range.transform}" != "${tr.transform}"`);
        }
        _updateArgs(range, tr);
      }
    }
  }
  ret.forEach(_padArgs);
  return ret;
}
