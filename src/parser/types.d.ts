
declare interface ParseCtx {
  index: number,
  value: string,
  unit?: string
}
