// @ts-check

import { makeError, parseList, skipSpaces } from './utils';

export const TType = Object.freeze({
  color: { canInterpolate: true },
  number: { canInterpolate: true },
  string: { canInterpolate: false }
});

const _strEscapeRe = new RegExp('\\\\.', 'g');
/**
 * @param {RegExpMatchArray} match
 */
function _strEscapeRepl(match) {
  return match[1];
}

/**
 * @param  {ParseCtx} ctx
 * @return {string}
 */
export function parseString(ctx) {
  const value = ctx.value;
  const delim = skipSpaces(ctx);

  if (delim !== '"' && delim !== "'") {
    throw new SyntaxError(makeError('failed to parse value: invalid string delimiter', ctx));
  }
  for (var i = ctx.index + 1; i < value.length; ++i) {
    var c = value[i];
    if (c === '\\') {
      ++i;
    }
    else if (c === delim) {
      const ret = value.slice(ctx.index + 1, i)// @ts-ignore: not sure why
      .replace(_strEscapeRe, _strEscapeRepl);
      ctx.index = ++i;
      return ret;
    }
  }
  throw new SyntaxError(makeError('failed to parse unterminated string value', ctx));
}

/*
  this expression does match corrupted things like "+" or the empty string,
  but this will be checked by parseScalar
 */
const _scalarRe = new RegExp('^\\s*([+-]?\\d*(?:\\.\\d*)?)\\s*([A-Za-z]*|%)?');

/**
 * @param  {ParseCtx} ctx
 * @return {number}
 */
export function parseScalar(ctx) {
  var match = ctx.value.slice(ctx.index).match(_scalarRe);
  if (!match) {
    throw new SyntaxError(makeError('failed to parse scalar value', ctx));
  }
  var value = parseFloat(match[1]);
  if (isNaN(value)) {
    throw new SyntaxError(makeError('failed to parse scalar value', ctx));
  }
  if (match[2]) {
    if (ctx.unit && (ctx.unit !== match[2])) {
      throw new SyntaxError(makeError('mixed units in scalar list', ctx));
    }
    ctx.unit = match[2];
  }
  ctx.index += match[0].length;
  return value;
}

/**
 * @param {string} value
 * @return {ssvg.$TRange|null} value
 */ // eslint-disable-next-line complexity
export function parseTRange(value) {
  /** @type {ParseCtx} */
  const ctx = { index: 0, value: value };
  /** @type {any} */
  var c = value ? skipSpaces(ctx) : null;
  if (c === null) {
    return null;
  }

  c = c.charCodeAt(0);
  if ((c === 34) || (c === 39)) { /* " | ' */
    return { type: 'string', values: parseList(ctx, parseString) };
  }
  else if ((c === 43) || (c === 45) || (c === 46) || ((c >= 48) && (c <= 57))) { /* +|-|.|[0-9] */
    var values = parseList(ctx, parseScalar);
    return (ctx.unit) ?
      { type: 'number', unit: ctx.unit, values: values } :
      { type: 'number', values: values };
  }
  else {
    return { type: 'color', values: ctx.value.split(';').map((s) => s.trim()) };
  }
}

/**
 * @param {string} value
 * @return {ssvg.$TValue|null}
 */
// eslint-disable-next-line complexity
export function parseTValue(value) {
  /** @type {ParseCtx} */
  const ctx = { index: 0, value: value };
  /** @type {any} */
  var c = value ? skipSpaces(ctx) : null;
  if (c === null) {
    return null;
  }

  c = c.charCodeAt(0);
  if ((c === 34) || (c === 39)) { /* " | ' */
    const str = parseString(ctx);
    if (skipSpaces(ctx) !== null) {
      throw new SyntaxError(makeError('invalid trailing characters in string', ctx));
    }
    return { type: 'string', value: str };
  }
  else if ((c === 43) || (c === 45) || (c === 46) || ((c >= 48) && (c <= 57))) { /* +|-|.|[0-9] */
    const scalar = parseScalar(ctx);
    if (skipSpaces(ctx) !== null) {
      throw new SyntaxError(makeError('invalid trailing characters in scalar', ctx));
    }
    return (ctx.unit) ?
      { type: 'number', unit: ctx.unit, value: scalar } :
      { type: 'number', value: scalar };
  }
  else {
    return { type: 'color', value: ctx.value };
  }
}

/**
 * @param {string} value
 * @return {string|number|boolean|null}
 */
// eslint-disable-next-line complexity
export function parseInitial(value) {
  /** @type {ParseCtx} */
  const ctx = { index: 0, value: value };
  /** @type {any} */
  var c = value ? skipSpaces(ctx) : null;
  if (c === null) {
    return value;
  }

  c = c.charCodeAt(0);
  if ((c === 34) || (c === 39)) { /* " | ' */
    const str = parseString(ctx);
    if (skipSpaces(ctx) !== null) {
      throw new SyntaxError(makeError('invalid trailing characters in string', ctx));
    }
    return str;
  }
  else if ((c === 43) || (c === 45) || (c === 46) || ((c >= 48) && (c <= 57))) { /* +|-|.|[0-9] */
    return parseFloat(ctx.value);
  }
  else {
    const lvalue = value.toLowerCase();
    if (lvalue === 'true') {
      return true;
    }
    else if (lvalue === 'false') {
      return false;
    }
    return value;
  }
}

/**
 * @param {ssvg.$TValue|null|undefined} from
 * @param {ssvg.$TValue|null|undefined} to
 * @return {ssvg.$TRange}
 */
export function makeRange(from, to) {
  if (!from || !to) {
    throw new SyntaxError('can create range with undefined values');
  }

  const ret = {
    values: [ from.value, to.value ],
    unit: from.unit,
    type: from.type
  };
  if (to.unit) {
    if (from.unit && (to.unit !== from.unit)) {
      throw new SyntaxError(`from/to unit differs: ${from.unit} !== ${to.unit}`);
    }
    ret.unit = to.unit;
  }
  if (from.type !== to.type) {
    throw new SyntaxError(`from/to type differs: ${from.type} !== ${to.type}`);
  }
  return ret;
}
