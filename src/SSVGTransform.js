// @ts-check

import { scaleLinear } from 'd3-scale';

import { AttributeType, CalcMode } from './SSVGRelationElement';
import { default as SSVGRelation, scaleThreshold } from './SSVGRelation';
import { normalizeTransform, parseTTransform, parseTTransformRange } from './parser/transform';
import { isNil } from './utils';

/**
 * @typedef {import('./SSVGState').default} SSVGState
 * @typedef {import('./SSVGProperty').default} SSVGProperty
 */


export default class SSVGTransform extends SSVGRelation {
  /**
   * @param {SSVGState} ssvg
   * @param {Element} element
   */
  constructor(ssvg, element) {
    super(ssvg, element);

    /** @type {ssvg.$TransformRange[]} */
    this.transforms; /* eslint-disable-line no-unused-expressions *//* jshint ignore:line */

    /** @type {number[]} */
    this.keyTimes; /* eslint-disable-line no-unused-expressions *//* jshint ignore:line */
  }

  init() {
    this.animName = 'ssvg-trans-' + this.id;
    this.canInterpolate = true;
    if (isNil(this.attributeName)) {
      this.attributeName = 'transform';
    }

    const steps = (this.transforms.length > 0) ? this.transforms[0].args.length : 0;
    if (!this.keyTimes) {
      /** @type {number[]} */
      this.keyTimes = [];
      for (let i = 0; i < steps; ++i) {
        this.keyTimes.push(i / (steps - 1));
      }
    }

    const values = [];
    const mapper = function(/** @type {number} */ step, /** @type {ssvg.$TransformRange} */range) { return range.args[step]; };
    for (let step = 0; step < steps; ++step) {
      values.push(this.transforms.map(mapper.bind(null, step)));
    }
    this.scale = /** @type {any} */ (scaleLinear(this.keyTimes, values));

    if (this.calcMode === CalcMode.discrete) {
      const toScale = scaleThreshold(this.keyTimes, this.keyTimes);
      this.quantize = (/** @type {SSVGProperty} */ prop, /** @type {number?=} */ t) => toScale(prop.normalize(t));
    }
  }

  /**
   * @param {Element} elt
   * @return {boolean}
   */
  _parseValues(elt) {
    const values = parseTTransformRange(elt.getAttribute('values') || '');
    if (!values) { return false; }

    this.transforms = normalizeTransform(values);
    return true;
  }

  /**
   * @param {Element} elt
   * @return {boolean}
   */
  _parseFrom(elt) {
    const from = elt.getAttribute('from');
    const to = elt.getAttribute('to');

    if (!to) { return false; }

    this.transforms = normalizeTransform([
      parseTTransform(from ? from : this._getCurrentValue()) || [],
      // @ts-ignore
      parseTTransform(to)
    ]);
    return true;
  }

  /**
   * @param {Element} elt
   * @return {boolean}
   */
  _parseBy(elt) {
    const by = elt.getAttribute('by');

    /* not yet a number */
    if (!by) { return false; }

    const current = this._getCurrentValue();
    const to = parseTTransform(by);
    if (!to) {
      throw new SyntaxError('failed to parse by value');
    }
    this.transforms = normalizeTransform([
      // @ts-ignore
      current ? parseTTransform(current) : [],
      to
    ]);
    for (const transform of this.transforms) {
      for (let i = 0; i < transform.args[0].length; ++i) {
        transform.args[1][i] += transform.args[0][i];
      }
    }
    return true;
  }

  /**
   * @param {Element} elt
   * @return {CalcMode}
   */
  _parseCalcMode(elt) {
    const calcMode = elt.getAttribute('calc-mode');
    if (!calcMode) {
      return CalcMode.linear;
    }
    else if (calcMode in CalcMode) {
      // @ts-ignore
      return CalcMode[calcMode];
    }
    else {
      throw new SyntaxError('invalid calcMode: ' + calcMode);
    }
  }

  /**
   * @brief computed target value from normalized input
   * @param  {number} n
   */
  strict(n) {
    this._last = n;
    const values = this.scale(n);
    return this.transforms.map(// @ts-ignore
      (tr, idx) => this.transformToString(tr, values[idx])).join(" ");
  }

  /**
   * @brief pre-computed target value
   * @param  {any} value
   */
  direct(value /*: any */) {
    this._last = value;
    return this.transforms.map(
      (tr, idx) => this.transformToString(tr, value[idx])).join(" ");
  }

  /**
   * @param {ssvg.$TransformRange} trRange
   * @param {number[]} values
   * @return {string}
   */
  transformToString(trRange, values) {
    if (values.length === 0) {
      return trRange.transform + "()";
    }

    return trRange.transform + "(" + values.map((value, idx) => {
      const unit = trRange.units[idx];
      return unit ? (value.toString() + unit) : value.toString();
    }).join(',') + ")";
  }

  /**
   * @return {string}
   */
  _getCurrentValue() {
    if (this.attributeName === '') {
      return this.selection.text() || '';
    }
    else if (!this.attributeName || (this.attributeType === AttributeType.XML)) {
      return this.selection.attr(this.attributeName || "transform") || '';
    }
    else {
      return this.selection.style(this.attributeName) || '';
    }
  }
}
