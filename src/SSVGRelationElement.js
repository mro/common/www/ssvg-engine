// @ts-check

import { TType, makeRange, parseTRange, parseTValue } from './parser/values';
import { isCSS } from './CSS';
import { default as SSVGTransition } from './SSVGTransition';

/**
 * @typedef {import('./SSVGState').default} SSVGState
 * @typedef {import('./SSVGProperty').default} SSVGProperty
 */

/** @enum {number} */
export const AttributeType = {
  XML: 0,
  CSS: 1,
  AUTO: 2
};

/** @enum {number} */
export const CalcMode = {
  linear: 0,
  discrete: 1
};

export default class SSVGRelationElement {
  /*::
  ssvg: SSVGState
  querySelector: string
  attributeName: ?string
  attributeType: $Values<typeof AttributeType>
  values: $TRange
  keyTimes: ?Array<number>
  calcMode: $Values<typeof CalcMode>
  transition: SSVGTransition
  selection: d3$Selection
  */
  /**
   * @param {SSVGState} ssvg
   * @param {Element} element
   */
  constructor(ssvg, element) {
    this.ssvg = ssvg;
    this.querySelector = element.getAttribute('query-selector') || '';
    this.selection = this.ssvg.selection.selectAll(this.querySelector);
    this.attributeName = element.getAttribute('attribute-name');
    this.attributeType = this._parseAttrType(element);

    /** @type {ssvg.$TRange} */
    this.values; /* eslint-disable-line no-unused-expressions *//* jshint ignore:line */
    if (!(this._parseValues(element) ||
          this._parseFrom(element) ||
          this._parseBy(element))) {
      throw new SyntaxError('at least values/from or by must be set');
    }

    this.keyTimes = this._parseKeyTimes(element);
    this.calcMode = this._parseCalcMode(element);
    this.transition = new SSVGTransition(element, ssvg.defaultTransition);

    this.element = element;
  }

  /**
   * @param {Element} element
   * @return {AttributeType}
   */
  _parseAttrType(element) {
    const type = element.getAttribute('attribute-type');
    if (!type || type === 'auto') {
      return (this.attributeName && isCSS(this.attributeName)) ?
        AttributeType.CSS : AttributeType.XML;
    }
    else if (type in AttributeType) {
      // @ts-ignore
      return AttributeType[type];
    }
    else {
      throw new SyntaxError('invalid attribute-type: ' + type);
    }
  }

  /**
   * @param {Element} elt
   */
  _parseKeyTimes(elt) {
    const keyTimes = elt.getAttribute('key-times');
    if (!keyTimes) { return null; }
    var range = parseTRange(keyTimes);
    if (!range) { return null; }
    if ((range.type !== 'number') ||
        (range.values[0] !== 0) ||
        (range.values[range.values.length - 1] !== 1)) {
      throw new SyntaxError('invalid keyTimes value :' + keyTimes);
    }
    return range.values;
  }

  /**
   * @param {Element} elt
   * @return {CalcMode}
   */
  _parseCalcMode(elt) {
    if (this.values && !TType[this.values.type].canInterpolate) {
      return CalcMode.discrete;
    }
    const calcMode = elt.getAttribute('calc-mode');
    if (!calcMode) {
      return CalcMode.linear;
    }
    else if (calcMode in CalcMode) {
      // @ts-ignore
      return CalcMode[calcMode];
    }
    else {
      throw new SyntaxError('invalid calcMode: ' + calcMode);
    }
  }

  /**
   * @param {Element} elt
   * @return {boolean}
   */
  _parseValues(elt) {
    const values = parseTRange(elt.getAttribute('values') || '');
    if (!values) { return false; }

    this.values = values;
    return true;
  }

  /**
   * @param {Element} elt
   * @return {boolean}
   */
  _parseFrom(elt) {
    const from = elt.getAttribute('from');
    const to = elt.getAttribute('to');

    /* not yet a number */
    if (!to) { return false; }

    this.values = makeRange(
      parseTValue(from ? from : this._getCurrentValue()),
      parseTValue(to));
    return true;
  }

  /**
   * @param {Element} elt
   * @return {boolean}
   */
  _parseBy(elt) {
    const by = elt.getAttribute('by');

    /* not yet a number */
    if (!by) { return false; }

    const current = parseTValue(this._getCurrentValue());
    const to = parseTValue(by);
    if (!to) {
      throw new SyntaxError('failed to parse by value');
    }
    else if (!current) {
      throw new SyntaxError('failed to parse retrieve current value');
    }
    // @ts-ignore
    to.value += current.value;
    this.values = makeRange(current, to);
    return true;
  }

  _getCurrentValue() {
    if (!this.attributeName) {
      return this.selection.text() || '';
    }
    else if (this.attributeType === AttributeType.CSS) {
      return this.selection.style(this.attributeName) || '';
    }
    else {
      return this.selection.attr(this.attributeName) || '';
    }
  }
}
