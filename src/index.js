// @ts-check

import SSVGCustomElement from './SSVGCustomElement';
export { default as SSVGEngine } from './SSVGEngine';


const plugin = SSVGCustomElement.register;
export { plugin };

import * as parser from './parser';
export { parser };

export { isCSS } from './CSS';
