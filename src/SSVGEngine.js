// @ts-check

import { select } from 'd3-selection';
import d from 'debug';
const debug = d('ssvg:engine');

import SSVGState from './SSVGState';

export default class SSVGEngine {
  /**
   * @param {Element} svg
   */
  constructor(svg /*: Element */) {
    debug('loading ssvg engine');
    this.svg = svg;
    /** @type {ssvg.StateSelection} */
    this.selection = /** @type {any} */ (select(svg));
    /** @type {SSVGState[]} */
    this.states = [];

    const states = svg.getElementsByTagName('state');
    for (let i = 0; i < states.length; ++i) {
      this.states.push(new SSVGState(this, states[i]));
    }
  }

  disconnect() {
    for (const s of this.states) {
      s.disconnect();
    }
  }

  /** @param {ssvg.$State} state */
  updateState(state) {
    for (const s of this.states) {
      s.updateState(state);
    }
  }

  /** @param {ssvg.$State} state */
  setState(state) {
    for (const s of this.states) {
      s.setState(state);
    }
  }

  /** @return {ssvg.$State} */
  getState() {
    if (this.states.length === 0) {
      return {};
    }
    return this.states[0].getState();
  }
}
