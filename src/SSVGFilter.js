// @ts-check

import { parseDomCallback } from './parser';
import { defaultTo } from './utils';

/**
 * @brief filter class
 * @details used to handle onvalue attributes or properties and filter target
 * values.
 */
class SSVGFilter {
  /**
   * @param {Element} element [description]
   */
  constructor(element) {
    this.element = element;
    /** @type {any} */
    this._value = undefined;
    /** @type {any} */
    this._result = undefined;

    const attr = this.element.getAttribute('onupdate');
    if (attr) {
      this._onupdate = parseDomCallback(attr, [ '$value', '$unit' ],
        defaultTo(element.ownerDocument, document).defaultView || undefined);
    }
  }

  /**
   * @param  {ssvg.$Value} value
   * @param  {string} [unit]
   * @return {string|number}
   */
  filter(value, unit) {
    if (value === this._value) {
      return this._result;
    }
    this._value = value;
    // @ts-ignore: added to the DOM
    if (this.element.onupdate) {
      // @ts-ignore: added to the DOM
      this._result = this.element.onupdate(value, unit);
    }
    else if (this._onupdate) {
      this._result = this._onupdate.call(this.element, value, unit);
    }
    else {
      this._result = (unit) ? (value + unit) : value;
    }
    return this._result;
  }
}

export default SSVGFilter;
