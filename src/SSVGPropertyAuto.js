// @ts-check

import { PropertyType, default as SSVGProperty } from './SSVGProperty';
import { isNil } from './utils';
import { parseInitial } from './parser';

/**
 * @typedef {import('./SSVGState').default} SSVGState
 */

export default class SSVGPropertyAuto extends SSVGProperty {
  /**
   * @param {SSVGState} ssvg
   * @param {Element} element
   */
  constructor(ssvg, element) {
    super(ssvg, element);

    this._itype = PropertyType.auto; // will be updated
    this.min = Number.parseFloat(element.getAttribute('min') || '0');
    this.max = Number.parseFloat(element.getAttribute('max') || '1');
    this._irange = { min: this.min, max: this.max };

    /** @type {any} */
    this._value; /* eslint-disable-line no-unused-expressions *//* jshint ignore:line */

    this._parseInitial(element.getAttribute('initial'));

    if (Number.isNaN(this.min)) {
      throw new SyntaxError('invalid min value for number property: ' +
        (element.getAttribute('min') || ''));
    }
    else if (Number.isNaN(this.max)) {
      throw new SyntaxError('invalid max value for number property: ' +
        (element.getAttribute('max') || ''));
    }
    for (let i = 0; i < this.relations.length; ++i) {
      this.relations[i].setInitial(this);
    }
  }

  /**
   * @param  {string?=} value
   */
  _parseInitial(value) {
    if (isNil(value)) {
      this._value = this._irange.min;
      this._itype = PropertyType.number;
    }
    else {
      this._value = parseInitial(value);
      if (Number.isFinite(this._value)) {
        this._itype = PropertyType.number;
        if (this._value < this.min) {
          this.min = this._value;
        }
        else if (this._value > this.max) {
          this.max = this._value;
        }
      }
      else if (this._value === true || this._value === false) {
        this._itype = PropertyType.boolean;
      }
      else {
        this._itype = PropertyType.enum;
      }
    }
  }

  /**
   * @param {any?=} [value]
   * @return {number}
   */
  normalize(value) {
    switch (this._itype) {
    case PropertyType.boolean:
      if (isNil(value)) {
        return this._value ? 1 : 0;
      }
      return value ? 1 : 0;
    case PropertyType.enum:
    default:
      return 0;
    case PropertyType.number:
      return this._normalizeNumber(value);
    }
  }

  /**
   * @param {number?=} [value]
   * @return {number}
   */
  _normalizeNumber(value) {
    if (this.min === this.max) {
      return this.min;
    }
    else if (!isNil(value)) {
      // @ts-ignore
      return (Number.parseFloat(value) - this.min) / (this.max - this.min);
    }
    else if (isNil(this._normalized)) {
      this._normalized = (this._value - this.min) / (this.max - this.min);
    }
    return this._normalized;
  }


  /** @param {number|any} value */
  set value(value) {
    this._normalized = null;
    if (Number.isFinite(value)) {
      if (this._itype !== PropertyType.number) {
        this.min = this._irange.min;
        this.max = this._irange.max;
      }
      if (value < this.min) {
        this.min = value;
      }
      else if (value > this.max) {
        this.max = value;
      }
      this._itype = PropertyType.number;
      super.value = value;
    }
    else if (value === true || value === false) {
      this._itype = PropertyType.boolean;
      super.value = value;
    }
    else {
      this._itype = PropertyType.enum;
      super.value = value;
    }
  }

  /** @return {number|any} */
  get value() {
    return super.value;
  }
}
