// @ts-check

import { scaleLinear } from 'd3-scale';
import { AttributeType, CalcMode } from './SSVGRelationElement';
import SSVGRelation from './SSVGRelation';
import { TransitionType } from './SSVGTransition';

/**
 * @typedef {import('./SSVGState').default} SSVGState
 * @typedef {import('./SSVGProperty').default} SSVGProperty
 */

export default class SSVGDirect extends SSVGRelation {
  init() {
    this.animName = 'ssvg-direct-' + this.id;
  }

  /**
   * @param {SSVGProperty} prop
   */
  setInitial(prop) {
    this.selection.interrupt(this.animName);
    const value = this.direct(prop.value);
    if (!this.attributeName) {
      this.selection.text(value);
    }
    else if (this.attributeType === AttributeType.CSS) {
      this.selection.style(this.attributeName, value);
    }
    else {
      this.selection.attr(this.attributeName, value);
    }
  }

  /**
   * @param {SSVGProperty} prop
   */
  update(prop) {
    this.selection.interrupt(this.animName);

    const to = prop.value;
    var t = this.selection.transition(this.animName);
    if (this.transition.delay) {
      t = t.delay(this.transition.delay);
    }
    t = t.ease(this.transition.timingFunction);

    t = t.duration(this.transition.duration);

    /** @type {(t: number) => any} */
    let interpolator;
    // interpolatable target value
    if (Number.isFinite(to) && Number.isFinite(this._last)) {
      interpolator = scaleLinear([ 0, 1 ], [ this._last, to ]);
    }
    else if (this.transition.type === TransitionType.direct) {
      // non-interpolatable direct transitions change value at half animation
      // duration
      interpolator = (/** @type {number} */t) => ((t >= 0.5) ? to : this._last);
    }
    else {
      // scaleThreshold simplification
      interpolator = (/** @type {number} */t) => ((t >= 1) ? to : this._last);
    }

    /** @type {(t: number) => any} */
    const factory = (t) => this.direct(interpolator(t));

    if (!this.attributeName) {
      t.textTween(() => factory);
    }
    else if (this.attributeType === AttributeType.CSS) {
      t.styleTween(this.attributeName, () => factory);
    }
    else {
      t.attrTween(this.attributeName, () => factory);
    }
  }

  /**
   * @brief pre-computed target value
   * @param {ssvg.$Value} value
   */
  direct(value) {
    this._last = value;
    return this._filter.filter(value);
  }

  /**
   * @brief unused
   * @param {number} n
   */
  strict(n) { return n; }

  _parseValues() { return true; }

  _parseCalcMode() { return CalcMode.linear; }
}
