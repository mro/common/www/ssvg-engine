// @ts-check

/**
 * @param  {string|null} duration
 * @return {number|null}
 */
export function parseDuration(duration) {
  if (!duration) { return null; }
  duration = duration.trim();
  const value = Number.parseFloat(duration);
  if (Number.isNaN(value)) {
    throw new SyntaxError('failed to parse duration: ' + duration);
  }
  return (duration.endsWith('ms')) ? value : (value * 1000);
}
