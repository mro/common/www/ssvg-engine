// @ts-check

import { cubicBezier } from './cubicBezier';
import { argSepRe } from '../utils';

/** @type {{ [name: string]: (t: number) => number }} */
export const builtinTimingFunctions = {
  linear: function(/** @type {number} */ t) { return +t; },
  ease: cubicBezier(0.25, 0.1, 1.25, 1),
  'ease-in': cubicBezier(0.42, 0, 1, 1),
  'ease-out': cubicBezier(0, 0, 0.58, 1),
  'ease-in-out': cubicBezier(0.42, 0, 0.58, 1)
};

/**
 * @param {string} tfunc
 * @param {string} str
 * @return {number}
 */
function _parseFloat(tfunc, str) {
  const num = Number.parseFloat(str);
  if (Number.isNaN(num)) {
    throw new SyntaxError('invalid number in timing function: ' + tfunc);
  }
  return num;
}

/**
 * @param  {string|null|undefined} tfunc
 * @return {((t: number) => number) | null}
 */
export function parseTimingFunction(tfunc) {
  if (!tfunc) { return null; }
  /*:: (tfunc: string) */

  tfunc = tfunc.trim();
  const idx = tfunc.indexOf('(');
  /** @type {((t: number) => number)|undefined} */
  var ret;

  if (idx === -1) {
    ret = builtinTimingFunctions[tfunc];
  }
  else if ((tfunc.slice(0, idx) === 'cubic-bezier') &&
           (tfunc[tfunc.length - 1] === ')')) {
    /** @type {any[]} */
    var values = tfunc.slice(idx + 1, -1).split(argSepRe);
    values = values.map(_parseFloat.bind(null, tfunc));
    if (values.length === 4) {
      // @ts-ignore
      return cubicBezier(...values);
    }
  }
  // @ts-ignore
  if (!ret) {
    throw new SyntaxError('invalid timing function: ' + tfunc);
  }
  return ret;
}
