// @ts-check

import SSVGTransition from './SSVGTransition';
import { createProperty } from './SSVGPropertyFactory';
import SSVGComputed from './SSVGComputed';

import d from 'debug';
const debug = d('ssvg');

/**
 * @typedef {import('./SSVGEngine').default} SSVGEngine
 * @typedef {import('./SSVGProperty').default} SSVGProperty
 */

export default class SSVGState {
  /**
   * @param {SSVGEngine} engine
   * @param {Element} state
   */
  constructor(engine, state) {
    debug('loading state');
    /** @type {{ [name: string]: SSVGProperty }} */
    this.properties = {};
    /** @type {SSVGComputed[]} */
    this.computed = [];
    /** @type {ssvg.StateElement} */
    this.elt = /** @type {any} */ (state);
    this.svg = engine.svg;
    this.selection = engine.selection;
    /** @type {CustomEvent<{ state: ssvg.$State | null }>} */
    this._evt = new CustomEvent('state', { 'state': null });

    this.defaultTransition = new SSVGTransition();
    this.defaultTransition.setDefaults();

    this._parseState(state);
    // $FlowIgnore: we're extending it !
    this.elt.updateState = this.updateState.bind(this);
    // $FlowIgnore: we're extending it !
    this.elt.setState = this.setState.bind(this);
    // $FlowIgnore: we're extending it !
    this.elt.getState = this.getState.bind(this);
  }

  disconnect() {
    // @ts-ignore: unmounting
    delete this.elt.updateState;

    // @ts-ignore: unmounting
    delete this.elt.setState;

    // @ts-ignore: unmounting
    delete this.elt.getState;

    for (const key in this.properties) {
      if (!this.properties.hasOwnProperty(key)) {
        continue;
      }
      this.properties[key].disconnect();
    }

    for (const computed of this.computed) {
      computed.disconnect();
    }
  }

  /**
   * @param {Element} state
   */
  _parseState(state) {
    if (state.firstElementChild &&
        (state.firstElementChild.nodeName === 'transition')) {
      this.defaultTransition.parseTransition(state.firstElementChild);
    }

    for (let i = 0; i < state.children.length; ++i) {
      var children = state.children[i];
      if (children.tagName === 'property') {
        const prop = createProperty(this, children);
        this.properties[prop.name] = prop;
      }
      else if (children.tagName === 'computed') {
        const prop = createProperty(this, children);
        this.computed.push(new SSVGComputed(prop, children));
      }
    }
  }

  /**
   * @param {ssvg.$State} state
   */
  updateState(state) {
    /** @type {ssvg.$State} */
    var event = {};

    for (const key in this.properties) {
      if (!this.properties.hasOwnProperty(key)) {
        continue;
      }
      else if (state.hasOwnProperty(key)) {
        this.properties[key].value = state[key];
      }
      event[key] = this.properties[key].value;
    }
    this._updateComputed(event);
    this._emitEvent(event);
  }

  /**
   * @param {ssvg.$State} state
   */
  setState(state) {
    /** @type {ssvg.$State} */
    const event = {};

    for (const key in this.properties) {
      if (!this.properties.hasOwnProperty(key)) {
        continue;
      }

      this.properties[key].value = state[key];
      event[key] = this.properties[key].value;
    }

    this._updateComputed(event);
    this._emitEvent(event);
  }

  /**
   * @param {ssvg.$State} event
   */
  _updateComputed(event) {
    if (this.computed.length === 0) {
      return;
    }
    for (const computed of this.computed) {
      computed.compute(event);
      event[computed.property.name] = computed.property.value;
    }
  }

  /**
   * @param {ssvg.$State} state
   */
  _emitEvent(state) {
    this._evt.state = state;
    this.elt.dispatchEvent(this._evt);
  }

  /** @return {ssvg.$State} */
  getState() {
    /** @type {ssvg.$State} */
    var ret = {};
    for (const key in this.properties) {
      if (!this.properties.hasOwnProperty(key)) {
        continue;
      }

      ret[key] = this.properties[key].value;
    }

    for (const computed of this.computed) {
      ret[computed.property.name] = computed.property.value;
    }
    return ret;
  }
}
