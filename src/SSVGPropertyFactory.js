// @ts-check

import SSVGPropertyBoolean from './SSVGPropertyBoolean';
import SSVGPropertyNumber from './SSVGPropertyNumber';
import SSVGPropertyEnum from './SSVGPropertyEnum';
import SSVGPropertyAuto from './SSVGPropertyAuto';

/**
 * @typedef {import('./SSVGState').default} SSVGState
 * @typedef {import('./SSVGProperty').default} SSVGProperty
 * @typedef {typeof import('./SSVGProperty').default} SSVGPropertyType
 */

/** @type {{ [type: string]: SSVGPropertyType }} */
const _registry = {
  boolean: SSVGPropertyBoolean,
  number: SSVGPropertyNumber,
  enum: SSVGPropertyEnum,
  auto: SSVGPropertyAuto
};

/**
 * @param {SSVGState} ssvg
 * @param {Element} element
 * @return {SSVGProperty}
 */
export function createProperty(ssvg, element) {
  const type = element.getAttribute('type');
  if (!type) {
    throw new SyntaxError('property type missing');
  }
  else if (!(type in _registry)) {
    throw new SyntaxError('invalid property type: ' + type);
  }
  return new _registry[type](ssvg, element);
}
