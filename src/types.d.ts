
import { Selection } from 'd3-selection'

// extend ssvg with private types
declare global {
namespace ssvg {

  interface StateElement extends Element {
    updateState(state: $State): void
    setState(state: $State): void
    getState(): $State
  }

  type StateSelection = Selection<StateElement, any, any, any>
}
}
