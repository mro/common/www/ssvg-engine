// @ts-check

import { parseDuration } from './timing/Duration';
import { builtinTimingFunctions, parseTimingFunction } from './timing/TimingFunction';

import { defaultTo, isNil } from './utils';

/** @enum {number} */
export const TransitionType = {
  direct: 0,
  strict: 1
};

export default class SSVGTransition {
  /**
   * @param {Element|null} [parent]
   * @param {SSVGTransition|null} [defaults]
   */
  constructor(parent, defaults) {
    this.defaults = defaults || this;

    if (parent) {
      for (let i = 0; i < parent.children.length; ++i) {
        const transition = parent.children[i];
        if (transition.tagName === 'transition') {
          this.parseTransition(transition);
        }
      }
    }

    /** @type {number?=} */
    this._duration; /* eslint-disable-line no-unused-expressions *//* jshint ignore:line */
    /** @type {((t: number) => number)?=} */
    this._timingFunction; /* eslint-disable-line no-unused-expressions *//* jshint ignore:line */
    /** @type {number?=} */
    this._delay; /* eslint-disable-line no-unused-expressions *//* jshint ignore:line */
    /** @type {TransitionType} */
    this._type; /* eslint-disable-line no-unused-expressions *//* jshint ignore:line */
  }

  setDefaults() {
    this._duration = 0;
    this._timingFunction = builtinTimingFunctions.linear;
    this._delay = 0;
    this._type = TransitionType.strict;
  }

  /**
   * @param {Element} elt
   */
  parseTransition(elt) {
    this._duration = defaultTo(parseDuration(elt.getAttribute('duration')), this._duration);
    this._timingFunction = defaultTo(parseTimingFunction(elt.getAttribute('timing-function')), this._timingFunction);
    this._delay = defaultTo(parseDuration(elt.getAttribute('delay')), this._delay);

    const type = elt.getAttribute('type');
    if (type) {
      if (type in TransitionType) {
        // @ts-ignore
        this._type = TransitionType[type];
      }
      else {
        throw new SyntaxError('invalid transition type: ' + type);
      }
    }
  }

  /** @return {number} */
  get duration() {
    // @ts-ignore
    return isNil(this._duration) ? this.defaults._duration : this._duration;
  }

  /** @return {(t: number) => number} */
  get timingFunction() {
    // @ts-ignore
    return isNil(this._timingFunction) ?
      this.defaults._timingFunction : this._timingFunction;
  }

  /** @return {number} */
  get delay() {
    // @ts-ignore
    return isNil(this._delay) ? this.defaults._delay : this._delay;
  }

  /** @return {TransitionType} */
  get type() {
    return isNil(this._type) ? this.defaults._type : this._type;
  }

  /** @return {boolean} */
  get isImmediate() { return (this.duration > 0) || (this.delay > 0); }
}
