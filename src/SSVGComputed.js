// @ts-check

import { parseDomCallback } from './parser/utils';
import { defaultTo } from './utils';
import d from 'debug';

const debug = d('ssvg:computed');

/**
 * @typedef {import('./SSVGProperty').default} SSVGProperty
 */

export default class SSVGComputed {
  /**
   * @param {SSVGProperty} property
   * @param {Element} element
   */
  constructor(property, element) {
    this.property = property;
    this.element = element;

    const attr = this.element.getAttribute('onupdate');
    if (attr) {
      this._onupdate = parseDomCallback(attr, [ '$state' ],
        defaultTo(element.ownerDocument, document).defaultView || undefined);
    }
  }

  disconnect() {
    this.property.disconnect();
  }

  /**
   * @param {ssvg.$State} state
   */
  compute(state) {
    try {
      // @ts-ignore: added to the DOM
      if (this.element.onupdate) {
        // @ts-ignore: added to the DOM
        this.property.value = this.element.onupdate(state);
      }
      else if (this._onupdate) {
        this.property.value = this._onupdate.call(this.element, state);
      }
      else {
        debug('onupdate not set on %s', this.property.name);
      }
    }
    catch (ex) {
      debug("failed to compute on %s: %s", this.property.name, ex);
    }
  }
}
