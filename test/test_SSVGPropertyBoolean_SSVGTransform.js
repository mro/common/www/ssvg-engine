// @ts-check
import "./karma_index";

import { after, before, describe, it } from 'mocha';
import { expect } from 'chai';

import SSVGEngine from '../src/SSVGEngine';
import { runSteps } from './utils';

import { select } from 'd3-selection';

/*::
import fixture from 'karma-fixture'
*/

describe('SSVGPropertyBoolean', function() {
  describe('SSVGTransform', function() {
    /** @type {Element} */
    var svg;
    /** @type {SSVGEngine} */
    var engine;

    before(function() {
      fixture.load('svg-property-boolean-transform.pug');
      // $FlowIgnore: tested
      svg/*: any */ = document.getElementById('svg');
      expect(svg).to.exist();

      engine = new SSVGEngine(svg);
    });

    after(function() {
      engine.disconnect();
    });

    [
      {
        name: 'applies relative linear transform (by)',
        selector: () => select('#rect1').attr('transform'),
        steps: [
          { state: { range: true }, result: 'rotate(45)' },
          { state: { range: false }, result: 'rotate(-45)' },
          { state: { range: true }, result: 'rotate(45)' }
        ]
      },
      {
        name: 'applies relative linear transform (to)',
        selector: () => select('#rect2').attr('transform'),
        steps: [
          { state: { range: true }, result: 'rotate(90)' },
          { state: { range: false }, result: 'rotate(45)' },
          { state: { range: true }, result: 'rotate(90)' }
        ]
      },
      {
        name: 'applies relative linear default transform (by)',
        selector: () => select('#rect3').attr('transform'),
        steps: [
          { state: { range: true }, result: 'rotate(90)' },
          { state: { range: false }, result: 'rotate(0)' },
          { state: { range: true }, result: 'rotate(90)' }
        ]
      },
      {
        name: 'applies relative linear default transform (to)',
        selector: () => select('#rect4').attr('transform'),
        steps: [
          { state: { range: true }, result: 'rotate(90)' },
          { state: { range: false }, result: 'rotate(0)' },
          { state: { range: true }, result: 'rotate(90)' }
        ]
      }
    ].forEach(function(test) {
      it(test.name, () => runSteps(engine, test));
    });
  });
});
