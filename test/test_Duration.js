// @ts-check
import "./karma_index";

import { describe, it } from 'mocha';
import { expect } from 'chai';

import { parseDuration } from '../src/timing/Duration';

describe('Duration', function() {
  it('can parse Duration', function() {
    [
      { str: '10s', result: 10000 },
      { str: '10.10ms', result: 10.10 },
      { str: "  1  s  ", result: 1000 },
      { str: " 0 ", result: 0 },
      { str: " .1", result: 100 },

      { str: null, result: null },
      { str: '', result: null },
      { str: undefined, result: null }
    ].forEach((test) => {
      // $FlowIgnore: using null/undefined on purpose
      expect(parseDuration(test.str)).to.deep.equal(test.result);
    });
  });

  it('throws an error', function() {
    [
      { str: 'invalid', msg: 'failed to parse' }
    ].forEach((test) => {
      expect(() => parseDuration(test.str)).to.throw(SyntaxError, test.msg);
    });
  });
});
