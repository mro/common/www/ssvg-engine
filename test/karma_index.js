// @ts-check

/* must be done early */
import debug from 'debug';
// @ts-ignore
debug.useColors = () => false;

/**
 * @typedef {import('karma-fixture')} fixture
 */

import chai from 'chai';
import { before } from 'mocha';
import dirtyChai from 'dirty-chai';

chai.use(dirtyChai);

localStorage['debug'] = 'ssvg*';

before(function() {
  fixture.setBase('test/fixtures');
});
