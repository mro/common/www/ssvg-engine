// @ts-check
import "./karma_index";

import { describe, it } from 'mocha';
import { expect } from 'chai';

import { parseTRange } from '../src/parser/values';

describe('TRange', function() {
  it('can parse ranges', function() {
    [
      { str: '"te;st";  \'to;\\\'to\'', result: { type: 'string', values: [ 'te;st', 'to;\'to' ] } },

      { str: '-1.1;.5;1%; 100 %', result: { type: 'number', unit: '%', values: [ -1.1, 0.5, 1, 100 ] } },
      { str: '100px', result: { type: 'number', values: [ 100 ], unit: 'px' } },

      { str: '0; 25; 100', result: { type: 'number', values: [ 0, 25, 100 ] } },

      { str: '#AA2233AA; blue', result: { type: 'color', values: [ '#AA2233AA', 'blue' ] } },

      { str: null, result: null },
      { str: '', result: null },
      { str: undefined, result: null }
    ].forEach((test) => {
      // $FlowIgnore: using null/undefined on purpose
      expect(parseTRange(test.str)).to.deep.equal(test.result);
    });
  });

  it('throws on error', function() {
    [
      { str: '"test" \'toto\'', msg: 'list separator' },
      { str: '"test" , \'toto\'', msg: 'list separator' },
      { str: '"test";\'toto;"toto"', msg: 'unterminated' },

      { str: '1%;100 pt', msg: 'unit' },

      /* mixed types */
      { str: '"test"; 100px', msg: undefined },

      /* invalid scalar (re mismatch) */
      { str: '-%', msg: undefined },

      /* invalid scalar */
      { str: '--1', msg: undefined }
    ].forEach((test) => {
      expect(() => parseTRange(test.str)).to.throw(SyntaxError, test.msg);
    });
  });
});
