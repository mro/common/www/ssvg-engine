// @ts-check
import "./karma_index";

import { after, before, describe, it } from 'mocha';
import { expect } from 'chai';

import SSVGEngine from '../src/SSVGEngine';
import { runSteps, waitForValue } from './utils';

import { select } from 'd3-selection';

/*::
import fixture from 'karma-fixture'
*/

describe('SSVGPropertyNumber', function() {
  describe('SSVGRelation', function() {
    /** @type {Element} */
    var svg;
    /** @type {SSVGEngine} */
    var engine;

    before(function() {
      fixture.load('svg-property-number-onupdate.pug');
      svg = document.getElementById('svg');
      expect(svg).to.exist();

      engine = new SSVGEngine(svg);
    });

    after(function() {
      engine.disconnect();
    });

    [
      {
        name: 'applies onupdate rule with string template',
        selector: () => select('#text').text(),
        steps: [
          { state: { range: 100 }, result: 'value: 50 unit: %' },
          { state: { range: 50 }, result: 'value: 25 unit: %' },
          { state: { range: 0 }, result: 'value: 0 unit: %' }
        ]
      },
      {
        name: 'applies onupdate rule with function',
        selector: () => select('#text2').text(),
        steps: [
          { state: { range: 100 }, result: '5.00e+1' },
          { state: { range: 50 }, result: '2.50e+1' },
          { state: { range: 0 }, result: '0.00e+0' }
        ]
      }
    ].forEach(function(test) {
      it(test.name, () => runSteps(engine, test));
    });

    it('calls onupdate only when necessary', async function() {
      const rel = engine.svg.querySelector('#relation3');
      expect(rel).to.exist();
      /** @type {any[]} */
      const values = [];
      // @ts-ignore part of the extension
      rel.onupdate = (/** @type {any} */ value) => {
        values.push(value);
        return value;
      };

      engine.updateState({ range: 100 });
      await waitForValue(() => select('#text2').attr('fill'), "blue");

      // ensure filter has been called only twice
      expect(values).to.deep.equal([ "green", "blue" ]);
    });
  });
});
