// @ts-check
import "./karma_index";

import { describe, it } from 'mocha';
import { expect } from 'chai';

import { parseTTransform } from '../src/parser/transform';

describe('TTransform', function() {
  it('can parse values', function() {
    [
      { str: 'rotate(90)', result: [ { transform: 'rotate', units: [ null ], args: [ 90 ] } ] },
      { str: '\n  rotate(90)', result: [ { transform: 'rotate', units: [ null ], args: [ 90 ] } ] },
      { str: '\n  rotate\n(90\n)', result: [ { transform: 'rotate', units: [ null ], args: [ 90 ] } ] },
      { str: 'rotate(90%, 60 deg)', result: [ { transform: 'rotate', units: [ '%', 'deg' ], args: [ 90, 60 ] } ] },
      { str: 'rotate(90, 60 deg), translate(100px 0) scale(1.1)',
        result: [
          { transform: 'rotate', units: [ null, 'deg' ], args: [ 90, 60 ] },
          { transform: 'translate', units: [ 'px', null ], args: [ 100, 0 ] },
          { transform: 'scale', units: [ null ], args: [ 1.1 ] }
        ]
      }
    ].forEach((test) => {
      // $FlowIgnore: using null/undefined on purpose
      expect(parseTTransform(test.str)).to.deep.equal(test.result);
    });
  });

  it('throws on error', function() {
    [
      { str: 'rotate(90', msg: 'unterminated' },
      { str: 'rotate(#1234)', msg: 'scalar' },
      { str: '(12)', msg: 'unnamed' },
      { str: 'rotate(12),,', msg: 'invalid char' },
      { str: 'rotate', msg: 'arguments' },
      { str: 'rotate,', msg: 'invalid char' },
      { str: 'translate(12),rotate ,', msg: 'arguments' },
      { str: 'rotate(12%) ;translate(12)', msg: 'separator' }
    ].forEach((test) => {
      expect(() => parseTTransform(test.str)).to.throw(SyntaxError, test.msg);
    });
  });
});
