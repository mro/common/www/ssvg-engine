// @ts-check
import "./karma_index";

import { after, before, describe, it } from 'mocha';
import { expect } from 'chai';

import SSVGEngine from '../src/SSVGEngine';
import { runSteps } from './utils';

import { select } from 'd3-selection';
import { rgb } from 'd3-color';

/*::
import fixture from 'karma-fixture'
*/

describe('SSVGPropertyAuto', function() {
  /** @type {Element} */
  var svg;
  /** @type {SSVGEngine} */
  var engine;

  before(function() {
    fixture.load('svg-property-auto.pug');
    svg = document.getElementById('svg');
    expect(svg).to.exist();

    engine = new SSVGEngine(svg);
  });

  after(function() {
    engine.disconnect();
  });

  [
    {
      name: 'handles numeric types',
      selector: () => select('#rect').style('width'),
      steps: [
        { state: { num: 5 }, result: '50%' },
        { state: { num: 10 }, result: '100%' },
        { state: { num: 20 }, result: '100%' },
        { state: { num: 10 }, result: '50%' },
        { state: { num: true }, result: '100%' },
        /* range is reset to default */
        { state: { num: 0.5 }, result: '50%' }
      ]
    },
    {
      name: 'handles numeric types with min/max',
      selector: () => select('#rect2').style('width'),
      steps: [
        { state: { num2: 70 }, result: '50%' },
        { state: { num2: 220 }, result: '100%' },
        { state: { num2: 120 }, result: '50%' },
        { state: { num2: true }, result: '100%' },
        /* range is reset to default */
        { state: { num2: 40 }, result: '20%' }
      ]
    },
    {
      name: 'handles boolean types',
      selector: () => select('#text').text(),
      steps: [
        { state: { bool: false }, result: 'false' },
        { state: { bool: true }, result: 'true' },
        { state: { bool: "carrot" }, result: 'carrot' },
        { state: { bool: false }, result: 'false' }
      ]
    },
    {
      name: 'handles enum types',
      selector: () => select('#text2').text(),
      steps: [
        { state: { enum: "test" }, result: 'test' },
        { state: { enum: "toto" }, result: 'toto' },
        { state: { enum: true }, result: 'true' }
      ]
    },
    {
      name: 'handles enum normalization',
      selector: () => select('#text2').attr('fill'),
      steps: [
        { state: { enum: "test" }, result: rgb('red').toString() },
        { state: { enum: true }, result: rgb('blue').toString() },
        { state: { enum: "toto" }, result: rgb('red').toString() }
      ]
    },
    {
      name: 'handles auto properties without initial value',
      selector: () => select('#text').attr('fill'),
      steps: [
        { state: { noInit: "test" }, result: rgb('red').toString() },
        { state: { noInit: true }, result: rgb('blue').toString() }
      ]
    },
    {
      name: 'can be used with computed properties',
      selector: () => select('#rect3').style('width'),
      steps: [
        /* clearing range */
        { state: { num2: 'papaya' }, result: '0%' },
        { state: { num2: 69 }, result: '70%' },
        { state: { num2: 199 }, result: '100%' },
        /* clearing range */
        { state: { num2: 'papaya' }, result: '0%' },
        { state: { num2: 59 }, result: '60%' }
      ]
    }
  ].forEach(function(test) {
    it(test.name, () => runSteps(engine, test));
  });

  it('handles initial value', function() {
    engine.disconnect();
    engine = new SSVGEngine(svg);
    const state = engine.getState();
    expect(state).to.deep.equal({
      num: 10, num2: 20, bool: true, enum: "true", noInit: 0, auto: 0
    });
  });
});
