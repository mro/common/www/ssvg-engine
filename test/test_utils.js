// @ts-check
import "./karma_index";

import { after, before, describe, it } from 'mocha';
import { expect } from 'chai';

import { parseDomCallback } from '../src/parser/utils';

describe('parseDomCallback', function() {

  before(function() {
    window.eventCallback = function(event) { return event; };
  });

  after(function() {
    delete window.eventCallBack;
  });

  it('can parse functions', function() {
    [
      "$state",
      "$state;",
      "(i = 1, $state)",
      "function() { return $state; }",
      "function f() { return $state }",
      "function(e) { return e; }",
      "function f($state) { return $state }",
      "window.eventCallback",
      "window.eventCallback($state)",
      "eventCallback($state)",
      "eventCallback",
      "(e) => e",
      "(e) => $state"
    ].forEach((test) => {
      var fun = parseDomCallback(test, [ "$state" ]);
      expect(fun).to.be.a('function');
      expect(fun(42)).to.equal(42);
      expect(fun("toto")).to.equal("toto");
    });
  });

  it('can parse functions with several arguments', function() {
    [
      "`${$value} ${$unit}`",
      "(value, unit) => `${value} ${unit}`",
      "() => ($value + ' ' + $unit)"
    ].forEach((test) => {
      var fun = parseDomCallback(test, [ "$value", "$unit" ]);
      expect(fun).to.be.a('function');
      expect(fun('10', 'cm')).to.equal('10 cm');
    });
  });
});
