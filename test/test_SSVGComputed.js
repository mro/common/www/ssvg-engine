// @ts-check
import "./karma_index";

import { after, before, describe, it } from 'mocha';
import { expect } from 'chai';

import SSVGEngine from '../src/SSVGEngine';
import { runSteps } from './utils';

import { select } from 'd3-selection';

/*::
import fixture from 'karma-fixture'
*/

describe('SSVGComputed', function() {
  /** @type {Element} */
  var svg;
  /** @type {SSVGEngine} */
  var engine;

  before(function() {
    fixture.load('ssvg-computed.pug');
    // @ts-ignore: tested
    svg/*: any */ = document.getElementById('svg');
    expect(svg).to.exist();

    engine = new SSVGEngine(svg);
  });

  after(function() {
    engine.disconnect();
  });

  [
    {
      name: 'can use an external function',
      selector: () => select('#rect').style('width'),
      steps: [
        { state: { range: 50 }, result: '51%' },
        { state: { range: 0 }, result: '1%' }
      ]
    },
    {
      name: 'can use an external function call',
      selector: () => select('#rect2').style('width'),
      steps: [
        { state: { range: 50 }, result: '51%' },
        { state: { range: 0 }, result: '1%' }
      ]
    },
    {
      name: 'can use an inline function',
      selector: () => select('#rect3').style('width'),
      steps: [
        { state: { range: 50 }, result: '51%' },
        { state: { range: 0 }, result: '1%' }
      ]
    }
  ].forEach(function(test) {
    it(test.name, () => runSteps(engine, test));
  });

  it('can use a DOM function', function() {
    const elt = document.getElementById('computeNumberDOM');
    expect(elt).to.exist();

    // @ts-ignore
    elt.onupdate = (state) => state.range + 1;
    return runSteps(engine, {
      selector: () => select('#rect4').style('width'),
      steps: [
        { state: { range: 50 }, result: '51%' },
        { state: { range: 0 }, result: '1%' }
      ]
    });
  });

  it('can override attributes with DOM functions', function() {
    // ensure that DOM takes precendence over inline
    const elt = document.getElementById('computeNumberInline');
    expect(elt).to.exist();

    // @ts-ignore
    elt.onupdate = (state) => state.range + 2;
    return runSteps(engine, {
      selector: () => select('#rect3').style('width'),
      steps: [
        { state: { range: 50 }, result: '52%' },
        { state: { range: 0 }, result: '2%' }
      ]
    });
  });

  it('can use "this" in inline functions', function() {
    const elt = document.getElementById('computeNumberThis');
    expect(elt).to.exist();

    // @ts-ignore
    elt.myProp = 42;
    return runSteps(engine, {
      selector: () => select('#rect5').style('width'),
      steps: [
        { state: { range: 50 }, result: '42%' },
        { state: { range: 0 }, result: '42%' }
      ]
    });
  });

  it('can use "this" in DOM functions', function() {
    const elt = document.getElementById('computeNumberThis');
    expect(elt).to.exist();

    // @ts-ignore
    elt.myProp2 = 44;
    // @ts-ignore
    elt.onupdate = function() { return this.myProp2 || 0; };
    return runSteps(engine, {
      selector: () => select('#rect5').style('width'),
      steps: [
        { state: { range: 50 }, result: '44%' },
        { state: { range: 0 }, result: '44%' }
      ]
    });
  });
});
