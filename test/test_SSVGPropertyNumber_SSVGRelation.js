// @ts-check
import "./karma_index";

import { after, before, describe, it } from 'mocha';
import { expect } from 'chai';

import SSVGEngine from '../src/SSVGEngine';
import { runSteps } from './utils';

import { select } from 'd3-selection';
import { rgb } from 'd3-color';

/*::
import fixture from 'karma-fixture'
*/

describe('SSVGPropertyNumber', function() {
  describe('SSVGRelation', function() {
    /** @type {Element} */
    var svg;
    /** @type {SSVGEngine} */
    var engine;

    before(function() {
      fixture.load('svg-property-number.pug');
      svg = document.getElementById('svg');
      expect(svg).to.exist();

      engine = new SSVGEngine(svg);
    });

    after(function() {
      engine.disconnect();
    });

    [
      {
        name: 'applies linear numeric relation',
        selector: () => select('#line').attr('stroke-width'),
        steps: [
          { state: { range: 100 }, result: '10' },
          { state: { range: 50 }, result: '5' },
          { state: { range: 0 }, result: '0' }
        ]
      },
      {
        name: 'applies linear numeric relation with units',
        selector: () => select('#rect').style('width'),
        steps: [
          { state: { range: 100 }, result: '50%' },
          { state: { range: 40 }, result: '20%' },
          { state: { range: 0 }, result: '0%' }
        ]
      },
      {
        name: 'applies linear named-color relation',
        selector: () => select('#rect').style('fill'),
        steps: [
          { state: { range: 100 }, result: 'rgb(0, 0, 255)' },
          { state: { range: 50 }, result: 'rgb(128, 0, 128)' },
          { state: { range: 0 }, result: 'rgb(255, 0, 0)' }
        ]
      },
      {
        name: 'applies discrete value relation',
        selector: () => select('#text').text(),
        steps: [
          { state: { range: 100 }, result: '4/4' },
          { state: { range: 74 }, result: '3/4' },
          { state: { range: 49 }, result: '2/4' },
          { state: { range: 24 }, result: '1/4' },
          { state: { range: 25 }, result: '2/4' },
          { state: { range: 0 }, result: '1/4' }
        ]
      },
      {
        name: 'applies non-linear numeric relation with units',
        selector: () => select('#rect2').style('width'),
        steps: [
          { state: { range: 0 }, result: '100%' },
          { state: { range: 25 }, result: '75%' },
          { state: { range: 50 }, result: '50%' },
          { state: { range: 75 }, result: '70%' },
          { state: { range: 100 }, result: '100%' },
          { state: { range: 0 }, result: '100%' }
        ]
      },
      {
        name: 'applies non-linear color relation',
        selector: () => select('#rect2').style('fill'),
        steps: [
          { state: { range: 0 }, result: rgb('pink').toString() },
          { state: { range: 25 }, result: 'rgb(0, 0, 255)' },
          { state: { range: 100 }, result: rgb('orange').toString() }
        ]
      },
      {
        name: 'applies discrete calc-mode on number',
        selector: () => select('#rect3').style('width'),
        steps: [
          { state: { range: 50 }, result: '50%' },
          { state: { range: 25 }, result: '0%' }
        ]
      },
      {
        name: 'applies discrete calc-mode on color',
        selector: () => select('#rect3').style('fill'),
        steps: [
          { state: { range: 50 }, result: rgb('orange').toString() },
          { state: { range: 25 }, result: rgb('red').toString() }
        ]
      },
      {
        name: 'applies relative linear numeric relation (by)',
        selector: () => select('#rect4').attr('width'),
        steps: [
          { state: { range: 100 }, result: '0%' },
          { state: { range: 0 }, result: '50%' },
          { state: { range: 50 }, result: '25%' }
        ]
      },
      {
        name: 'applies relative linear color relation (to)',
        selector: () => select('#rect4').style('fill'),
        steps: [
          { state: { range: 0 }, result: rgb('yellow').toString() },
          { state: { range: 100 }, result: rgb('pink').toString() },
          { state: { range: 0 }, result: rgb('yellow').toString() }
        ]
      }
    ].forEach(function(test) {
      it(test.name, () => runSteps(engine, test));
    });
  });
});
