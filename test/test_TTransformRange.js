// @ts-check
import "./karma_index";

import { describe, it } from 'mocha';
import { expect } from 'chai';

import { normalizeTransform, parseTTransformRange } from '../src/parser/transform';

describe('TTransformRange', function() {
  it('can parse values', function() {
    [
      { str: 'rotate(90)', result: [ [ { transform: 'rotate', units: [ null ], args: [ 90 ] } ] ] },
      { str: 'rotate(90);translate(10);scale(100),rotate(10)',
        result: [
          [ { transform: 'rotate', units: [ null ], args: [ 90 ] } ],
          [ { transform: 'translate', units: [ null ], args: [ 10 ] } ],
          [ { transform: 'scale', units: [ null ], args: [ 100 ] },
            { transform: 'rotate', units: [ null ], args: [ 10 ] } ]
        ]
      }
    ].forEach((test) => {
      expect(parseTTransformRange(test.str)).to.deep.equal(test.result);
    });
  });

  it('throws on error', function() {
    [
      { str: 'rotate(90);;', msg: 'invalid char' }
    ].forEach((test) => {
      expect(() => parseTTransformRange(test.str)).to.throw(SyntaxError, test.msg);
    });
  });

  it('can normalize transforms', function() {
    [
      { str: 'rotate(90);rotate(0)', result: [ { transform: 'rotate', units: [ null ], args: [ [ 90 ], [ 0 ] ] } ] },
      { str: 'rotate(90);rotate(100),translate(10, 10px);rotate(20%)',
        result: [
          { transform: 'rotate', units: [ '%' ], args: [ [ 90 ], [ 100 ], [ 20 ] ] },
          { transform: 'translate', units: [ null, 'px' ], args: [ [ 0, 0 ], [ 10, 10 ], [ 0, 0 ] ] }
        ]
      },
      { str: 'translate(10, 20px); translate(20px, 10)', // can detect units
        result: [
          { transform: 'translate', units: [ 'px', 'px' ], args: [ [ 10, 20 ], [ 20, 10 ] ] }
        ]
      },
      { str: 'rotate(90);  rotate(10%) ;   \t ',
        result: [
          { transform: 'rotate', units: [ '%' ], args: [ [ 90 ], [ 10 ], [ 0 ] ] }
        ]
      }
    ].forEach((test) => {
      expect(normalizeTransform(parseTTransformRange(test.str))).to.deep.equal(test.result);
    });
  });

  it('throws on normalization error', function() {
    [
      { str: 'rotate(90px);rotate(10deg)', msg: 'unit mismatch' },
      { str: 'rotate(90px);translate(10)', msg: 'name mismatch' }
    ].forEach((test) => {
      expect(() => normalizeTransform(parseTTransformRange(test.str))).to.throw(SyntaxError, test.msg);
    });
  });
});
