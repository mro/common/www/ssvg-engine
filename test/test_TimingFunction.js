// @ts-check
import "./karma_index";

import { describe, it } from 'mocha';
import { expect } from 'chai';

import {
  builtinTimingFunctions,
  parseTimingFunction } from '../src/timing/TimingFunction';

describe('TimingFunction', function() {

  it('can parse custom cubic-bezier functions', function() {
    const cubicBezier = parseTimingFunction('cubic-bezier(0.42, 0.0, 0.58, 1.0)');
    expect(cubicBezier).to.be.a('function');

    expect(cubicBezier(0.4)).to.be.closeTo(0.331884, 1e-6);
  });

  it('can parse builtin TimingFunction', function() {
    [
      { str: 'linear', result: builtinTimingFunctions.linear },
      { str: 'ease ', result: builtinTimingFunctions.ease },
      { str: " ease-in  ", result: builtinTimingFunctions['ease-in'] },
      { str: " ease-out ", result: builtinTimingFunctions['ease-out'] },
      { str: " ease-in-out ", result: builtinTimingFunctions['ease-in-out'] },

      { str: null, result: null },
      { str: '', result: null },
      { str: undefined, result: null }
    ].forEach((test) => {
      expect(parseTimingFunction(test.str)).to.deep.equal(test.result);
    });
  });

  it('throws on error', function() {
    [
      { str: 'lin ear', msg: 'invalid timing function' },
      { str: 'cubic-bezier (0, 0, 0)', msg: 'invalid timing function' }
    ].forEach((test) => {
      expect(() => parseTimingFunction(test.str)).to.throw(SyntaxError, test.msg);
    });
  });

});
