// @ts-check
import "./karma_index";

import { afterEach, describe, it } from 'mocha';
import { expect } from 'chai';

import { parser } from '../src';

/*::
import fixture from 'karma-fixture'
*/

describe('SSVGEngine', function() {
  describe('CSP', function() {
    afterEach(function() {
      // @ts-ignore
      delete window.testFunc;
    });

    it("doesn't use eval for direct functions", async function() {
      // @ts-ignore
      window.testFunc = () => 1;

      expect(parser.parseDomCallback("testFunc", [])) // @ts-ignore
      .to.equal(window.testFunc);
    });

    it("wraps functions using eval", async function() {
      // @ts-ignore
      window.testFunc = () => 1;

      expect(parser.parseDomCallback("testFunc()", [])) // @ts-ignore
      .to.not.equal(window.testFunc);
    });
  });
});
