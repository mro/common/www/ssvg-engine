// @ts-check
import "./karma_index";

import { describe, it } from 'mocha';
import { expect } from 'chai';

import { parseTValue } from '../src/parser/values';

describe('TValue', function() {
  it('can parse values', function() {
    [
      { str: '"test"', result: { type: 'string', value: 'test' } },
      { str: '"t\\"\\\\est"', result: { type: 'string', value: 't\"\\est' } },

      { str: "'test'", result: { type: 'string', value: 'test' } },
      { str: "'t\\'\\\\est'", result: { type: 'string', value: 't\'\\est' } },

      { str: '-1.1', result: { type: 'number', value: -1.1 } },
      { str: '.5', result: { type: 'number', value: 0.5 } },
      { str: '+1', result: { type: 'number', value: 1 } },
      { str: '+1 %', result: { type: 'number', value: 1, unit: '%' } },
      { str: '100px', result: { type: 'number', value: 100, unit: 'px' } },

      { str: '#112233', result: { type: 'color', value: '#112233' } },
      { str: 'blue', result: { type: 'color', value: 'blue' } },

      { str: null, result: null },
      { str: '', result: null },
      { str: undefined, result: null }
    ].forEach((test) => {
      expect(parseTValue(test.str)).to.deep.equal(test.result);
    });
  });

  it('throws on error', function() {
    [
      { str: '"test', msg: 'unterminated' },
      { str: "'test", msg: 'unterminated' },
      { str: "'test\\'", msg: 'unterminated' },
      { str: "'test' xx", msg: 'trailing' },

      { str: "1/1", msg: 'trailing' }

      // { str: "invalid", msg: 'failed' }
    ].forEach((test) => {
      expect(() => parseTValue(test.str)).to.throw(SyntaxError, test.msg);
    });
  });
});
