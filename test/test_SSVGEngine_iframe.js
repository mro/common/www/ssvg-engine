// @ts-check
import "./karma_index";

import { after, before, describe, it } from 'mocha';
import { expect } from 'chai';

import SSVGEngine from '../src/SSVGEngine';
import { waitFor, waitForValue } from './utils';

import { select } from 'd3-selection';

/*::
import fixture from 'karma-fixture'
*/

describe('SSVGEngine', function() {
  describe('IFrame', function() {
    /** @type {HTMLIFrameElement} */
    var iframe;
    /** @type {SSVGEngine} */
    var engine;

    before(function() {
      fixture.load('svg-iframe.pug');
      // @ts-ignore: tested
      iframe = document.getElementById('iframe');
      expect(iframe).to.exist();
    });

    after(function() {
      if (engine) {
        engine.disconnect();
      }
    });

    it('can use window binding in an iframe', async function() {
      const svg = await waitFor(
        () => iframe.contentDocument && iframe.contentDocument.getElementById('svg'));
      const engine = new SSVGEngine(svg);

      engine.updateState({ range: 10 });
      await waitForValue(
        () => select(svg).select('#rect').style('width'), '10%');
      await waitForValue(
        () => select(svg).select('#rect2').style('width'), '10%');
    });
  });
});
