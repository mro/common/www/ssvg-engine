// @ts-check
import "./karma_index";

import { after, before, describe, it } from 'mocha';
import { expect } from 'chai';

import SSVGEngine from '../src/SSVGEngine';

/*::
import fixture from 'karma-fixture'
*/

describe('SSVState', function() {
  /** @type {Element} */
  var svg;
  /** @type {SSVGEngine} */
  var engine;

  before(function() {
    fixture.load('svg-property-number.pug');
    svg = document.getElementById('svg');
    expect(svg).to.exist();

    engine = new SSVGEngine(svg);
  });

  after(function() {
    engine.disconnect();
  });

  it('emit event on state change', function(done) {
    const state = document.getElementById('state');
    expect(state).to.exist();

    var callback = function(event) {
      expect(event).to.have.nested.property('state.range', 12);
      state.removeEventListener('state', callback);
      done();
    };
    state.addEventListener('state', callback);

    engine.setState({ range: 12 });
  });

  it('emit event on state update', function(done) {
    const state = document.getElementById('state');
    expect(state).to.exist();

    var callback = function(event) {
      expect(event).to.have.nested.property('state.range', 12);
      state.removeEventListener('state', callback);
      done();
    };
    state.addEventListener('state', callback);

    engine.updateState({ range: 12 });
  });
});
