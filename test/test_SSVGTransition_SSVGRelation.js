// @ts-check
import "./karma_index";

import { after, before, describe, it } from 'mocha';
import { expect } from 'chai';

import SSVGEngine from '../src/SSVGEngine';
import { wait, waitForValue } from './utils';

import { select } from 'd3-selection';

/*::
import fixture from 'karma-fixture'
*/

describe('SSVGTransition', function() {
  describe('SSVGRelation', function() {
    /** @type {Element} */
    var svg;
    /** @type {SSVGEngine} */
    var engine;

    before(function() {
      fixture.load('svg-transition-relation.pug');
      svg = document.getElementById('svg');
      expect(svg).to.exist();

      engine = new SSVGEngine(svg);
    });

    after(function() {
      engine.disconnect();
    });

    [
      {
        name: 'supports strict mode',
        selector: () => select('#strict'),
        not: true
      },
      {
        name: 'defaults to strict mode',
        selector: () => select('#def'),
        not: true
      },
      {
        name: 'supports direct mode',
        selector: () => select('#direct')
      }
    ].forEach(function(test) {
      it(test.name, function() {
        var target = test.selector();
        engine.updateState({ range: 0 });
        return wait(100)
        .then(() => waitForValue(() => target.style('width'), '0%', 1000))
        .then(() => expect(target.style('width')).to.equal('0%')) // just for the symertry
        .then(() => engine.updateState({ range: 100 }))
        .then(() => wait(100))
        .then(() => {
          if (test.not) {
            expect(target.style('width')).to.not.equal('0%');
          }
          else {
            expect(target.style('width')).to.equal('0%');
          }
        })
        .then(() => waitForValue(() => target.style('width'), '0%', 1000));
      });
    });

    it('does not try to tween non-interpolatable values in direct mode', function() {
      /* when a non-interpolatable value looks like an integer it must still never be tweened */
      var target = select('#direct');
      engine.updateState({ range: 0 });
      return wait(500)
      .then(() => expect(target.style('fill')).to.equal('red')) // just for the symertry
      .then(() => engine.updateState({ range: 100 }))
      .then(() => wait(100))
      .then(() => expect(target.style('fill')).to.equal('red'))
      .then(() => waitForValue(() => target.style('fill'), 'blue', 1000));
    });
  });
});
