// @ts-check
import "./karma_index";

import { after, before, describe, it } from 'mocha';
import { expect } from 'chai';

import SSVGEngine from '../src/SSVGEngine';
import { wait, waitForValue } from './utils';

import { select } from 'd3-selection';

/*::
import fixture from 'karma-fixture'
import type { d3$Selection } from 'd3-selection'
*/

describe('SSVGPropertyNumber', function() {
  describe('SSVGRelation', function() {
    /** @type {Element} */
    var svg;
    /** @type {SSVGEngine} */
    var engine;

    before(function() {
      fixture.load('svg-property-number-ease.pug');
      // $FlowIgnore: tested
      svg/*: any */ = document.getElementById('svg');
      expect(svg).to.exist();

      engine = new SSVGEngine(svg);
    });

    after(function() {
      engine.disconnect();
    });

    function getValue(sel /*: d3$Selection */) {
      return Number.parseFloat(sel.style('width'));
    }

    [
      {
        name: 'applies ease timing-function',
        selector: () => select('#ease'),
        steps: [ 'lessThan', 'greaterThan' ]
      },
      {
        name: 'applies ease-in timing-function',
        selector: () => select('#ease-in'),
        steps: [ 'lessThan', 'greaterThan' ]
      },
      {
        name: 'applies ease-out timing-function',
        selector: () => select('#ease-out'),
        steps: [ 'greaterThan', 'lessThan' ]
      },
      {
        name: 'applies ease-in-out timing-function',
        selector: () => select('#ease-in-out'),
        steps: [ 'lessThan', 'lessThan' ]
      },
      {
        name: 'applies custom timing-function',
        selector: () => select('#custom'),
        steps: [ 'greaterThan', 'lessThan' ]
      }
    ].forEach(function(test) {
      it(test.name, function() {
        var linear = select('#linear'); // comparison value
        var target = test.selector();
        engine.updateState({ range: 100 });
        return wait(250)
        .then(() => expect(getValue(target))[test.steps[0]](getValue(linear)))
        .then(() => waitForValue(() => target.style('width'), '90%', 1000))
        .then(() => engine.updateState({ range: 0 }))
        .then(() => wait(750))
        .then(() => expect(getValue(target))[test.steps[1]](getValue(linear)))
        .then(() => waitForValue(() => target.style('width'), '10%', 1000));
      });
    });
  });
});
