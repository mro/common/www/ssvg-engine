// @ts-check
import "./karma_index";

import { after, before, describe, it } from 'mocha';
import { expect } from 'chai';

import SSVGEngine from '../src/SSVGEngine';
import { runSteps } from './utils';

import { select } from 'd3-selection';

/*::
import fixture from 'karma-fixture'
*/

describe('SSVGPropertyBoolean', function() {
  describe('SSVGRelation', function() {
    /** @type {Element} */
    var svg;
    /** @type {SSVGEngine} */
    var engine;

    before(function() {
      fixture.load('svg-property-boolean.pug');

      svg = document.getElementById('svg');
      expect(svg).to.exist();

      engine = new SSVGEngine(svg);
    });

    after(function() {
      engine.disconnect();
    });

    [
      {
        name: 'applies discrete value relation',
        selector: () => select('#text').text(),
        steps: [
          { state: { range: true }, result: 'ON' },
          { state: { range: false }, result: 'OFF' },
          { state: { range: true }, result: 'ON' }
        ]
      }
    ].forEach(function(test) {
      it(test.name, () => runSteps(engine, test));
    });

    it('sets initial value', function() {
      const state = engine.getState();
      expect(state).to.have.property('rangeFalse', false);
      expect(state).to.have.property('rangeZero', false);
      expect(state).to.have.property('rangeTrue', true);
      expect(state).to.have.property('rangeInvalidTrue', true);
    });
  });
});
