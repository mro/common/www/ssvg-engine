// @ts-check
import "./karma_index";

import { afterEach, beforeEach, describe, it } from 'mocha';
import { expect } from 'chai';

import SSVGEngine from '../src/SSVGEngine';
import { waitForValue } from './utils';

import { select } from 'd3-selection';

/*::
import fixture from 'karma-fixture'
*/

describe('SSVGPropertyNumber', function() {
  describe('load', function() {
    /** @type {Element} */
    var svg;
    /** @type {SSVGEngine} */
    var engine;

    beforeEach(function() {
      fixture.load('svg-property-number-load.pug');
      svg = document.getElementById('svg');
      expect(svg).to.exist();
    });

    afterEach(function() {
      if (engine) {
        engine.disconnect();
        // $FlowIgnore
        engine = null;
      }
    });

    /**
     * @param {Element} svg
     * @param {number} count
     */
    function createRects(svg /*: Element */, count /*: number */) {
      var sel = select(svg);

      for (var i = 0; i < count; ++i) {
        sel.append('rect')
        .attr('y', (i * 100 / count) + '%')
        .attr('height', (100 / count) + '%');
      }
      sel.append('text')
      .style('fill', 'black')
      .style('font-size', 'xx-large')
      .attr('y', '5%')
      .text('x' + count + ' items');
    }

    it('can animate 1k rect x2 rels', function() {
      createRects(svg, 1000);
      engine = new SSVGEngine(svg);

      var rect = select('rect');
      return Promise.resolve()
      .then(() => engine.updateState({ range: 100 }))
      .then(() => waitForValue(() => rect.style('width'), '100%', 1000))
      .then(() => engine.updateState({ range: 0 }))
      .then(() => waitForValue(() => rect.style('width'), '0%', 1000));
    });

    it('can animate 5k rect x2 rels', function() {
      createRects(svg, 5000);
      engine = new SSVGEngine(svg);

      var rect = select('rect');
      return Promise.resolve()
      .then(() => engine.updateState({ range: 100 }))
      .then(() => waitForValue(() => rect.style('width'), '100%', 1000))
      .then(() => engine.updateState({ range: 0 }))
      .then(() => waitForValue(() => rect.style('width'), '0%', 1000));
    });

    it('can animate 25k rect x2 rels', function() {
      createRects(svg, 25000);
      engine = new SSVGEngine(svg);

      var rect = select('rect');
      return Promise.resolve()
      .then(() => engine.updateState({ range: 100 }))
      .then(() => waitForValue(() => rect.style('width'), '100%', 1200))
      .then(() => engine.updateState({ range: 0 }))
      .then(() => waitForValue(() => rect.style('width'), '0%', 1200))
      .catch(() => console.log('failed for performance reasons'));
    });
  });
});
