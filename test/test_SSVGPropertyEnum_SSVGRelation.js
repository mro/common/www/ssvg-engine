// @ts-check
import "./karma_index";

import { after, before, describe, it } from 'mocha';
import { expect } from 'chai';

import SSVGEngine from '../src/SSVGEngine';
import { runSteps } from './utils';

import { select } from 'd3-selection';
import { rgb } from 'd3-color';

/*::
import fixture from 'karma-fixture'
*/

describe('SSVGPropertyEnum', function() {
  describe('SSVGRelation', function() {
    /** @type {Element} */
    var svg;
    /** @type {SSVGEngine} */
    var engine;

    before(function() {
      fixture.load('svg-property-enum.pug');
      svg = document.getElementById('svg');
      expect(svg).to.exist();

      engine = new SSVGEngine(svg);
    });

    after(function() {
      engine.disconnect();
    });

    [
      {
        name: 'applies discrete value relation',
        selector: () => select('#text').text(),
        steps: [
          { state: { range: "IDLE" }, result: 'idle' },
          { state: { range: "RUNNING" }, result: 'running' },
          { state: { range: "ERROR" }, result: 'error' },
          { state: { range: "IDLE" }, result: 'idle' }
        ]
      },
      {
        name: 'applies linear numeric relation with units',
        selector: () => select('#rect').style('width'),
        steps: [
          { state: { range: "IDLE" }, result: '0%' },
          { state: { range: "RUNNING" }, result: '100%' },
          { state: { range: "ERROR" }, result: '100%' },
          { state: { range: "IDLE" }, result: '0%' }
        ]
      },
      {
        name: 'applies non-linear named-color relation',
        selector: () => select('#rect').style('fill'),
        steps: [
          { state: { range: "IDLE" }, result: rgb('black').toString() },
          { state: { range: "RUNNING" }, result: rgb('green').toString() },
          { state: { range: "ERROR" }, result: rgb('red').toString() },
          { state: { range: "IDLE" }, result: rgb('black').toString() }
        ]
      },
      {
        name: 'applies discrete value relation with different ranges',
        selector: () => select('#text2').text(),
        steps: [
          { state: { range: "IDLE" }, result: 'ok' },
          { state: { range: "RUNNING" }, result: 'ok' },
          { state: { range: "ERROR" }, result: 'error' },
          { state: { range: "IDLE" }, result: 'ok' }
        ]
      }
    ].forEach(function(test) {
      it(test.name, () => runSteps(engine, test));
    });

    it('sets initial value', function() {
      const state = engine.getState();
      expect(state).to.have.property('rangeInit', 'RUNNING');
      expect(state).to.have.property('rangeInit2', 'RUNNING');
      expect(state).to.have.property('rangeSingle', 'IDLE');
      expect(state).to.have.property('rangeEmpty', '');
    });
  });
});
