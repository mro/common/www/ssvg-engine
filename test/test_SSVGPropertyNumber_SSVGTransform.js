// @ts-check
import "./karma_index";

import { after, before, describe, it } from 'mocha';
import { expect } from 'chai';

import SSVGEngine from '../src/SSVGEngine';
import { runSteps } from './utils';

import { select } from 'd3-selection';

/*::
import fixture from 'karma-fixture'
*/

describe('SSVGPropertyNumber', function() {
  describe('SSVGTransform', function() {
    /** @type {Element} */
    var svg;
    /** @type {SSVGEngine} */
    var engine;

    before(function() {
      fixture.load('svg-apc-logo.pug');
      svg = document.getElementById('svg');
      expect(svg).to.exist();

      engine = new SSVGEngine(svg);
    });

    after(function() {
      engine.disconnect();
    });

    [
      {
        name: 'applies CSS transforms',
        selector: () => select('path').style('transform'),
        steps: [
          { state: { range: 3 }, result: 'scale(2) rotateX(0deg) rotateZ(0deg) translateZ(0px)' },
          { state: { range: 2 }, result: 'scale(0.8) rotateX(60deg) rotateZ(-45deg) translateZ(80px)' },
          { state: { range: 1 }, result: 'scale(0.8) rotateX(60deg) rotateZ(-45deg) translateZ(0px)' },
          { state: { range: 2 }, result: 'scale(0.8) rotateX(60deg) rotateZ(-45deg) translateZ(80px)' },
          { state: { range: 3 }, result: 'scale(2) rotateX(0deg) rotateZ(0deg) translateZ(0px)' },
          { state: { range: 0 }, result: 'scale(1) rotateX(0deg) rotateZ(0deg) translateZ(0px)' }
        ]
      }
    ].forEach(function(test) {
      it(test.name, () => runSteps(engine, test));
    });

    it('can target textContent', function() {
      return runSteps(engine, {
        selector: () => select('#text').text(),
        steps: [
          { state: { range: 3 }, result: 'scale(2) rotateX(0deg) rotateZ(0deg) translateZ(0px)' },
          { state: { range: 0 }, result: 'scale(1) rotateX(0deg) rotateZ(0deg) translateZ(0px)' }
        ]
      });
    });
  });
});
