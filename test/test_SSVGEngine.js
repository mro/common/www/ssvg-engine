// @ts-check
import "./karma_index";

import { describe, it } from 'mocha';
import { expect } from 'chai';

import SSVGEngine from '../src/SSVGEngine';
import { waitFor } from './utils';

import { select } from 'd3-selection';

/*::
import fixture from 'karma-fixture'
*/

describe('SSVGEngine', function() {

  it('parse simple document', function() {
    fixture.load('svg-simple.pug');

    const svg = document.getElementById('svg');
    expect(svg).to.exist();

    const engine = new SSVGEngine(svg);

    const ex1 = document.getElementById('ex1');
    expect(ex1).to.exist();

    engine.updateState({ range: 10 });
    return waitFor(() => select('#ex1').style('width') === '100%', 1000)
    .then(() => engine.updateState({ range: 0 }))
    .then(() => waitFor(() => select('#ex1').style('width') === '0%', 1000));
  });

  it('disconnects properly', function() {
    fixture.load('svg-simple.pug');

    const svg = document.getElementById('svg');
    expect(svg).to.exist();

    const engine = new SSVGEngine(svg);

    engine.updateState({ range: 10 });
    return waitFor(() => select('#ex1').style('width') === '100%', 1000)
    .then(() => engine.updateState({ range: 0 }))
    .then(() => engine.disconnect())
    .then(() => waitFor(() => select('#ex1').style('width') === '0%', 1000))
    .then(
      () => { throw 'should fail'; },
      () => null
    );
  });
});
