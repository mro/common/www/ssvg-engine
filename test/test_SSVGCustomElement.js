// @ts-check
import "./karma_index";

import { before, describe, it } from 'mocha';
import { expect } from 'chai';
import { waitForValue } from './utils';

import { select } from 'd3-selection';

import SSVGCustomElement from '../src/SSVGCustomElement';

/**
 * @typedef {import('..').SSVGCustomElement} CustomElement
 * @typedef {import('..').SSVGStateElement} StateElement
 */

describe('SSVGCustomElement', function() {
  before(function() {
    SSVGCustomElement.register();
  });

  it('parse simple document', function() {
    fixture.load('ssvg-simple.html');

    /** @type {CustomElement} */
    const ssvg = /** @type {any} */ (document.getElementById('ssvg'));
    expect(ssvg).to.have.property('setState');

    /** @type {StateElement} */
    const state = /** @type {any} */ (document.getElementById('state'));
    expect(state).to.have.property('setState');
    expect(state).to.have.property('getState');
    expect(state.getState()).to.be.an('object');

    const target = select('#ex1');
    ssvg.setState(ssvg.getState());
    ssvg.setState({ range: 50 });
    ssvg.updateState({ range: 100 });

    return waitForValue(() => target.style('width'), '100%', 1000)
    .then(() => ssvg.disconnect());
  });
});
