// @flow
import "./karma_index";

import { describe, it } from 'mocha';
import { expect } from 'chai';

import { cubicBezier } from '../src/timing/cubicBezier';

describe('cubicBezier', function() {
  it("easeCubicBezier(0.42, 0.0, 0.58, 1.0)(t) returns the expected results", function() {
    const easeCubicBezier = cubicBezier(0.42, 0.0, 0.58, 1.0);

    expect(easeCubicBezier(0.0)).to.be.closeTo(0.000000, 1e-6);
    expect(easeCubicBezier(0.1)).to.be.closeTo(0.019722, 1e-6);
    expect(easeCubicBezier(0.2)).to.be.closeTo(0.081660, 1e-6);
    expect(easeCubicBezier(0.3)).to.be.closeTo(0.187396, 1e-6);
    expect(easeCubicBezier(0.4)).to.be.closeTo(0.331884, 1e-6);
    expect(easeCubicBezier(0.5)).to.be.closeTo(0.500000, 1e-6);
    expect(easeCubicBezier(0.6)).to.be.closeTo(0.668116, 1e-6);
    expect(easeCubicBezier(0.7)).to.be.closeTo(0.812604, 1e-6);
    expect(easeCubicBezier(0.8)).to.be.closeTo(0.918340, 1e-6);
    expect(easeCubicBezier(0.9)).to.be.closeTo(0.980278, 1e-6);
    expect(easeCubicBezier(1.0)).to.be.closeTo(1.000000, 1e-6);
  });

  it("easeCubicBezier(0.33, 0.95, 0.52, -0.18)(t) returns the expected results", function() {
    const easeCubicBezier = cubicBezier(0.33, 0.95, 0.52, -0.18);

    expect(easeCubicBezier(0.0)).to.be.closeTo(0.000000, 1e-6);
    expect(easeCubicBezier(0.1)).to.be.closeTo(0.235872, 1e-6);
    expect(easeCubicBezier(0.2)).to.be.closeTo(0.369957, 1e-6);
    expect(easeCubicBezier(0.3)).to.be.closeTo(0.419391, 1e-6);
    expect(easeCubicBezier(0.4)).to.be.closeTo(0.418897, 1e-6);
    expect(easeCubicBezier(0.5)).to.be.closeTo(0.410078, 1e-6);
    expect(easeCubicBezier(0.6)).to.be.closeTo(0.427126, 1e-6);
    expect(easeCubicBezier(0.7)).to.be.closeTo(0.490156, 1e-6);
    expect(easeCubicBezier(0.8)).to.be.closeTo(0.607032, 1e-6);
    expect(easeCubicBezier(0.9)).to.be.closeTo(0.778085, 1e-6);
    expect(easeCubicBezier(1.0)).to.be.closeTo(1.000000, 1e-6);
  });

  it("easeCubicBezier(0.0, 0.0, 1.0, 1.0)(t) returns the expected results", function() {
    var easeCubicBezier = cubicBezier(0.0, 0.0, 1.0, 1.0);

    expect(easeCubicBezier(0.0)).to.be.closeTo(0.0, 1e-6);
    expect(easeCubicBezier(0.1)).to.be.closeTo(0.1, 1e-6);
    expect(easeCubicBezier(0.2)).to.be.closeTo(0.2, 1e-6);
    expect(easeCubicBezier(0.3)).to.be.closeTo(0.3, 1e-6);
    expect(easeCubicBezier(0.4)).to.be.closeTo(0.4, 1e-6);
    expect(easeCubicBezier(0.5)).to.be.closeTo(0.5, 1e-6);
    expect(easeCubicBezier(0.6)).to.be.closeTo(0.6, 1e-6);
    expect(easeCubicBezier(0.7)).to.be.closeTo(0.7, 1e-6);
    expect(easeCubicBezier(0.8)).to.be.closeTo(0.8, 1e-6);
    expect(easeCubicBezier(0.9)).to.be.closeTo(0.9, 1e-6);
    expect(easeCubicBezier(1.0)).to.be.closeTo(1.0, 1e-6);
  });

  it("easeCubicBezier(bx, by, cx, cy)(t) clips bx and cx to [0.0, 1.0] interval", function() {
    const easeCubicBezier = cubicBezier(5.0, 0.0, -1.0, 1.0);
    const expected = cubicBezier(1.0, 0.0,  0.0, 1.0);

    expect(easeCubicBezier(0.0)).to.be.closeTo(expected(0.0), 1e-6);
    expect(easeCubicBezier(0.1)).to.be.closeTo(expected(0.1), 1e-6);
    expect(easeCubicBezier(0.2)).to.be.closeTo(expected(0.2), 1e-6);
    expect(easeCubicBezier(0.3)).to.be.closeTo(expected(0.3), 1e-6);
    expect(easeCubicBezier(0.4)).to.be.closeTo(expected(0.4), 1e-6);
    expect(easeCubicBezier(0.5)).to.be.closeTo(expected(0.5), 1e-6);
    expect(easeCubicBezier(0.6)).to.be.closeTo(expected(0.6), 1e-6);
    expect(easeCubicBezier(0.7)).to.be.closeTo(expected(0.7), 1e-6);
    expect(easeCubicBezier(0.8)).to.be.closeTo(expected(0.8), 1e-6);
    expect(easeCubicBezier(0.9)).to.be.closeTo(expected(0.9), 1e-6);
    expect(easeCubicBezier(1.0)).to.be.closeTo(expected(1.0), 1e-6);
  });
});
