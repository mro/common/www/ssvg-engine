// @ts-check

import "./karma_index";

import { after, before, describe, it } from 'mocha';
import { expect } from 'chai';

import SSVGEngine from '../src/SSVGEngine';
import { runSteps, waitForValue } from './utils';

import { select } from 'd3-selection';

/*::
import fixture from 'karma-fixture'
*/

describe('SSVGPropertyNumber', function() {
  describe('SSVGDirect', function() {
    /** @type {Element} */
    var svg;
    /** @type {SSVGEngine} */
    var engine;

    before(function() {
      fixture.load('svg-direct.pug');
      svg = document.getElementById('svg');
      expect(svg).to.exist();

      engine = new SSVGEngine(svg);
    });

    after(function() {
      engine.disconnect();
    });

    [
      {
        name: 'applies direct filtered value',
        selector: () => select('#rect').style('width'),
        steps: [
          { state: { range: 100 }, result: '100%' },
          { state: { range: 40 }, result: '40%' },
          { state: { range: 0 }, result: '0%' }
        ]
      },
      {
        name: 'applies direct unfiltered value',
        selector: () => select('#text').text(),
        steps: [
          { state: { range: 100 }, result: '100' },
          { state: { range: 74 }, result: '74' },
          { state: { range: 49 }, result: '49' },
          { state: { range: 24 }, result: '24' },
          { state: { range: 25 }, result: '25' },
          { state: { range: 0 }, result: '0' }
        ]
      }
    ].forEach(function(test) {
      it(test.name, () => runSteps(engine, test));
    });

    it('can use "this" on inline onupdate', async function() {
      // @ts-ignore
      document.getElementById('direct1').myProp = 42;
      engine.updateState({ range: 10 });
      await waitForValue(() => select('#line').attr('x2'), '42%');
    });

    it('can use "this" on DOM onupdate', async function() {
      /** @type {any} */
      const elt = document.getElementById('direct2');
      elt.myProp = 42;
      elt.onupdate = function() { return (this.myProp || 0) + '%'; };

      engine.updateState({ range: 10 });
      await waitForValue(() => select('#line').attr('x1'), '42%');
    });
  });
});
