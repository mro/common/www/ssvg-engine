// @ts-check

/**
 * @typedef {import('..').SSVGEngine} SSVGEngine
 * @typedef {{
 *   name?: string,
 *   selector: () => any,
 *   steps: Array<{ state: any, result: any }>
 * }} SteppedTest
 */

/**
 * @param {any} deferred
 * @param {() => boolean|any} test
 * @param {number} timeout
 */
function waitDeferred(deferred, test, timeout) {
  var ret = test();
  if (ret) {
    deferred.resolve(ret);
  }
  else if (Date.now() > timeout) {
    deferred.reject();
  }
  else {
    window.setTimeout(waitDeferred.bind(null, deferred, test, timeout), 100);
  }
}

/**
 * @param {() => boolean|any} test
 * @param {number} [timeout]
 * @return {Promise<any>}
 */
export function waitFor(test, timeout) {
  return new Promise(function(resolve, reject) {
    waitDeferred({ resolve, reject }, test, Date.now() + (timeout || 1000));
  });
}

/**
 * @param {() => boolean|any} test
 * @param {any} value
 * @param {number} [timeout]
 * @return {Promise<any>}
 */
export function waitForValue(test, value, timeout) {
  var _val;
  return waitFor(() => {
    _val = test();
    return _val === value;
  }, timeout)
  .catch(() => {
    throw new Error("Invalid result value: " + _val.toString() + " != " + value);
  });
}

/**
 * @param {SSVGEngine} engine
 * @param {SteppedTest} test
 */
export function runSteps(engine, test) {
  var p = Promise.resolve();
  test.steps.forEach((step) => {
    p = p
    .then(() => engine.updateState(step.state))
    .then(() => {
      return waitFor(() => test.selector() === step.result, 1000)
      .catch(() => {
        throw new Error("Invalid result value: " + test.selector() + " != " + step.result +
          " for input " + JSON.stringify(step.state));
      });
    });
  });
  return p;
}

/**
 * @param {number} ms
 * @return {Promise<void>}
 */
export function wait(ms) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}
