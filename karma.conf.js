const
  { karmaConfig } = require('@cern/karma-mocha-webpack');

module.exports = function(karma) {
  karma.set(karmaConfig(karma, {
    frameworks: [ 'mocha', 'chai', 'fixture' ],

    files: [
        'test/test_*.js',
        {
            // watch all files in src
            pattern: 'src/**',
            included: false,
            served: false,
            nocache: true
        },
        {
          pattern: 'test/fixtures/**',
          included: true
        },
        {
          pattern: 'test/dist/**',
          served: true
        }
    ],

    preprocessors: {
      'test/test_*.js': [ 'webpack', 'sourcemap' ],
      'test/fixtures/*.pug': [ 'pug', 'html2js' ],
      'test/fixtures/*.html': [ 'html2js' ],
      'test/dist/*.pug': [ 'pug' ]
    }
  }));
};
