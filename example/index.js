// @ts-check

const
  express = require('express'),
  path = require('path'),
  fs = require('fs'),
  pug = require('pug'),
  bodyParser = require('body-parser');

var config;
try {
  /* @ts-ignore */
  config = require('/etc/app/config'); /* eslint-disable-line global-require */
}
catch (e) {
  config = { basePath: '' };
}

const app = express();
const router = express.Router();

app.use(bodyParser.text({ type: 'text/plain' }));

app.set('view engine', 'pug');
app.set('views', path.join(__dirname, 'views'));

router.get('/', (req, res) => res.render('index', config));

fs.readdirSync(path.join(__dirname, 'views')).forEach((file) => {
  if (file.endsWith('.pug')) {
    var view = file.substring(0, file.length - 4);
    router.get('/' + view, (req, res) => res.render(view, config));
  }
});

router.use('/dist', express.static(path.join(__dirname, '..', 'dist')));
router.use('/vendor', express.static(path.join(__dirname, 'vendor')));

router.post('/pugRender', (req, res) => {
  res.setHeader('content-type', 'text/plain');
  res.send(pug.render(req.body));
});

app.use(config.basePath, router);

app.listen(8080, () => console.log('Server listening on port 8080'));
