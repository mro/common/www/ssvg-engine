
$(document).ready(() => {
  const ssvg = $('s-svg');
  const state = ssvg.find('state');
  const msgs = $('.x-messages');
  const msgCount = $('.x-messages-count');

  /* transform tabs in spaces */
  const extraKeys = {
    Tab: function(cm) {
      var spaces = Array(cm.getOption("indentUnit") + 1).join(" ");
      cm.replaceSelection(spaces);
    }
  };

  /* display error message */
  var count = 0;
  msgCount.hide();
  msgCount.removeClass('d-none');
  function message(text) {
    msgs.removeClass('d-none');
    msgs.append('<div class="alert alert-danger" role="alert"><pre>' + text + '</pre></div>');
    msgCount.text(++count);
    show(msgCount, true);
  }

  function show(btn, state) {
    if (state) {
      btn.show(400);
    }
    else {
      btn.hide(400);
    }
  }

  function initDeclCM() {
    const decl = $('.x-declaration');
    const btn = decl.find('.x-btn');
    var declCM = CodeMirror(decl.find('.x-cm').get(0), {
      value: state.text(), lineNumbers: true, mode: "pug",
      viewportMargin: Infinity, lineWrapping: true
    });
    declCM.setOption("extraKeys", extraKeys);

    function updateStateDecl() {
      show(btn, false);
      axios.post(basePath + '/pugRender',
        declCM.getValue(), { headers: { 'Content-Type': 'text/plain' } })
      .then(
        (res) => {
          state.html(res.data);
          var current = state.get(0).getState();
          try {
            ssvg.get(0).load();
            /* restore state */
            state.get(0).updateState(current);
          }
          catch (e) {
            message(e.toString());
          }
          $('.x-ssvg').removeClass('d-none');
        },
        (err) => message(err.toString())
      );
    }
    decl.find('.x-btn').on('click', updateStateDecl);
    btn.hide();
    btn.removeClass('d-none');
    updateStateDecl();
    declCM.on('change', () => show(btn, true));
  }
  initDeclCM();

  function initValueCM() {
    const val = $('.x-value');
    const btn = val.find('.x-btn');
    var valueCM = CodeMirror(val.find('.x-cm').get(0), {
      value: "", lineNumbers: true, mode: "javascript",
      viewportMargin: Infinity, lineWrapping: true
    });
    valueCM.setOption("extraKeys", extraKeys);

    var autoUpdate = false;
    state.on('state', function(e) {
      autoUpdate = true;
      valueCM.setValue(JSON.stringify(e.originalEvent.state, null, 2));
      autoUpdate = false;
    });
    valueCM.setValue(JSON.stringify(state.get(0).getState(), null, 2));
    btn.hide();
    btn.removeClass('d-none');
    val.find('.x-btn').on('click', () => {
      state.get(0).updateState(JSON.parse(valueCM.getValue()));
    });
    valueCM.on('change', () => show(btn, !autoUpdate));
  }
  initValueCM();

});
